#include <stdio.h>
#include <string.h>
#include <gsl/gsl_version.h>

int main (int argc, char **argv)
{
   int major, minor, patch;
   int n;

   (void) argc;
   (void) argv;

   if (0 != strcmp (GSL_VERSION, gsl_version))
     {
	fprintf (stderr, "\
****************************************************************\n\
ERROR: Installation or Configuration problem:\n\
\n\
   The GSL version defined by the library (%s) is not the same as\n\
   the one in the gsl_version.h header file (%s).  Please check the\n\
   Makefile variables and your GSL installation.\n\
\n\
****************************************************************\n",
		gsl_version, GSL_VERSION);
	return 1;
     }

   n = sscanf (gsl_version, "%d.%d.%d", &major, &minor, &patch);

   if (n < 3)
     {
	patch = 0;
	if (n < 2)
	  {
	     minor = 0;
	     if (n < 1)
	       {
		  fprintf (stderr, "Unsupported version of GSL: %s\n", gsl_version);
		  return 1;
	       }
	  }
     }

   fprintf (stdout, "\n#define GSL_VERSION_INT %d\n", major*10000 + minor * 100 + patch);

   return 0;
}

