prepend_to_slang_load_path (".");
set_import_module_path (".");

require ("gslsf");

static variable Callback_Called;
static define domain_callback (func, code)
{
   if (code != GSL_EDOM)
     {
	() = fprintf (stderr, "domain_callback: expecting GSL_EDOM\n");
	exit (1);
     }
   Callback_Called = 1;
}

gsl_set_error_disposition (GSL_EDOM, &domain_callback);
Callback_Called = 0;
() = log_1plusx (-10);
if (Callback_Called == 0)
{
   () = fprintf (stderr, "domain_callback NOT called\n");
   exit (1);
}


exit (0);

