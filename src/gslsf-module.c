/* -*- mode: C; mode: fold; -*- */
/* This file was automatically generated. */

/*
  Copyright (c) 2003-2011 Massachusetts Institute of Technology

  This software was developed by the MIT Center for Space Research
  under contract SV1-61010 from the Smithsonian Institution.

  Permission to use, copy, modify, distribute, and sell this software
  and its documentation for any purpose is hereby granted without fee,
  provided that the above copyright notice appear in all copies and
  that both that copyright notice and this permission notice appear in
  the supporting documentation, and that the name of the Massachusetts
  Institute of Technology not be used in advertising or publicity
  pertaining to distribution of the software without specific, written
  prior permission.  The Massachusetts Institute of Technology makes
  no representations about the suitability of this software for any
  purpose.  It is provided "as is" without express or implied warranty.

  THE MASSACHUSETTS INSTITUTE OF TECHNOLOGY DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE MASSACHUSETTS
  INSTITUTE OF TECHNOLOGY BE LIABLE FOR ANY SPECIAL, INDIRECT OR
  CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
  OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
  NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
  WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/* Author: John E. Davis (jed@jedsoft.org) */

#include <stdio.h>
#include <slang.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_version.h>

#include <gsl/gsl_sf_dilog.h>
#include <gsl/gsl_sf_laguerre.h>
#include <gsl/gsl_sf_fermi_dirac.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_sf_trig.h>
#include <gsl/gsl_sf_gegenbauer.h>
#include <gsl/gsl_sf_synchrotron.h>
#include <gsl/gsl_sf_zeta.h>
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_sf_psi.h>
#include <gsl/gsl_sf_lambert.h>
#include <gsl/gsl_sf_transport.h>
#include <gsl/gsl_sf_clausen.h>
#include <gsl/gsl_sf_hyperg.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_exp.h>
#include <gsl/gsl_sf_debye.h>
#include <gsl/gsl_sf_dawson.h>
#include <gsl/gsl_sf_expint.h>
#include <gsl/gsl_sf_coulomb.h>
#include <gsl/gsl_sf_mathieu.h>
#include <gsl/gsl_sf_legendre.h>
#include <gsl/gsl_sf_ellint.h>
#include <gsl/gsl_sf_airy.h>

#ifdef __cplusplus
extern "C"
{
#endif
/* SLANG_MODULE(gslsf); */
#ifdef __cplusplus
}
#endif

#include "slgsl.h"
#include "version.h"

#define MODULE_HAS_INTRINSICS
#define _GSLSF_MODULE_C_
#ifdef MODULE_HAS_INTRINSICS
/*{{{ Helper Functions */

#ifdef _GSLSF_MODULE_C_
static gsl_mode_t Default_GSL_Mode = GSL_PREC_SINGLE;

static int get_gsl_precision (void)
{
   return (int) Default_GSL_Mode;
}
static void set_gsl_precision (int *pp)
{
   int p = *pp;

   if ((p == GSL_PREC_SINGLE) || (p == GSL_PREC_DOUBLE) || (p == GSL_PREC_APPROX))
     Default_GSL_Mode = p;
}

static int get_gsl_mode (gsl_mode_t *mp, int from_stack)
{
   if (from_stack)
     {
	int mode;
	if (-1 == SLang_pop_integer (&mode))
	  return -1;
	*mp = (gsl_mode_t) mode;
     }

   *mp = Default_GSL_Mode;
   return 0;
}

static void do_d_dm (double (*f)(double, gsl_mode_t), gsl_mode_t m)
{
   SLGSL_Double_Array_Type a;
   SLang_Array_Type *in, *out;
   unsigned int i, n;
   double *xp, *yp;

   if (-1 == slgsl_pop_d_array (&a, 0))
     return;

   if (NULL == (in = a.at))
     {
	(void) SLang_push_double ((*f)(a.x, m));
	return;
     }

   if (NULL == (out = SLang_create_array (SLANG_DOUBLE_TYPE, 0, NULL, in->dims, in->num_dims)))
     {
	SLang_free_array (in);
	return;
     }

   n = in->num_elements;
   xp = a.xp;
   yp = (double *) out->data;
   for (i = 0; i < n; i++)
     yp[i] = (*f)(xp[i], m);

   (void) SLang_push_array (out, 1);
   SLang_free_array (in);
}

static void do_d_ddm (double (*f)(double, double, gsl_mode_t), gsl_mode_t m)
{
   SLGSL_Double_Array_Type a, b;
   SLang_Array_Type *atz;
   unsigned int i, n;
   double *xp, *yp, *zp;
   unsigned int xinc, yinc;

   if (-1 == slgsl_pop_dd_array (&a, &b, 0))
     return;

   if ((NULL == (atz = a.at))
       && (NULL == (atz = b.at)))
     {
	(void) SLang_push_double ((*f)(a.x, b.x, m));
	return;
     }

   atz = SLang_create_array (SLANG_DOUBLE_TYPE, 0, NULL, atz->dims, atz->num_dims);
   if (atz == NULL)
     {
	SLang_free_array (a.at);
	SLang_free_array (b.at);
	return;
     }

   n = atz->num_elements;
   zp = (double *) atz->data;
   xp = a.xp;
   yp = b.xp;
   xinc = a.inc;
   yinc = b.inc;

   for (i = 0; i < n; i++)
     {
	zp[i] = (*f)(*xp, *yp, m);
	xp += xinc;
	yp += yinc;
     }

   (void) SLang_push_array (atz, 1);
   SLang_free_array (a.at);
   SLang_free_array (b.at);
}

static void do_d_dddm (double (*f)(double, double, double, gsl_mode_t), gsl_mode_t m)
{
   SLGSL_Double_Array_Type a, b, c;
   SLang_Array_Type *atz;
   unsigned int i, n;
   double *ap, *bp, *cp, *zp;
   unsigned int ainc, binc, cinc;

   if (-1 == slgsl_pop_ddd_array (&a, &b, &c, 0))
     return;

   if ((NULL == (atz = a.at))
       && (NULL == (atz = b.at))
       && (NULL == (atz = c.at)))
     {
	(void) SLang_push_double ((*f)(a.x, b.x, c.x, m));
	return;
     }

   atz = SLang_create_array (SLANG_DOUBLE_TYPE, 0, NULL, atz->dims, atz->num_dims);
   if (atz == NULL)
     {
	SLang_free_array (a.at);
	SLang_free_array (b.at);
	SLang_free_array (c.at);
	return;
     }

   n = atz->num_elements;
   zp = (double *) atz->data;
   ap = a.xp;
   bp = b.xp;
   cp = c.xp;
   ainc = a.inc;
   binc = b.inc;
   cinc = c.inc;

   for (i = 0; i < n; i++)
     {
	zp[i] = (*f)(*ap, *bp, *cp, m);
	ap += ainc;
	bp += binc;
	cp += cinc;
     }

   (void) SLang_push_array (atz, 1);
   SLang_free_array (a.at);
   SLang_free_array (b.at);
   SLang_free_array (c.at);
}

static void do_d_ddddm (double (*f)(double, double, double, double, gsl_mode_t),
			gsl_mode_t m)
{
   SLGSL_Double_Array_Type a, b, c, d;
   SLang_Array_Type *atz;
   unsigned int i, n;
   double *ap, *bp, *cp, *dp, *zp;
   unsigned int ainc, binc, cinc, dinc;

   if (-1 == slgsl_pop_dddd_array (&a, &b, &c, &d, 0))
     return;

   if ((NULL == (atz = a.at))
       && (NULL == (atz = b.at))
       && (NULL == (atz = c.at))
       && (NULL == (atz = d.at)))
     {
	(void) SLang_push_double ((*f)(a.x, b.x, c.x, d.x, m));
	return;
     }

   atz = SLang_create_array (SLANG_DOUBLE_TYPE, 0, NULL, atz->dims, atz->num_dims);
   if (atz == NULL)
     {
	SLang_free_array (a.at);
	SLang_free_array (b.at);
	SLang_free_array (c.at);
	SLang_free_array (d.at);
	return;
     }

   n = atz->num_elements;
   zp = (double *) atz->data;
   ap = a.xp;
   bp = b.xp;
   cp = c.xp;
   dp = d.xp;
   ainc = a.inc;
   binc = b.inc;
   cinc = c.inc;
   dinc = d.inc;

   for (i = 0; i < n; i++)
     {
	zp[i] = (*f)(*ap, *bp, *cp, *dp, m);
	ap += ainc;
	bp += binc;
	cp += cinc;
	dp += dinc;
     }

   (void) SLang_push_array (atz, 1);
   SLang_free_array (a.at);
   SLang_free_array (b.at);
   SLang_free_array (c.at);
   SLang_free_array (d.at);
}

static void do_d_dm_fun (const char *fun, double (*f)(double, gsl_mode_t))
{
   gsl_mode_t m;

   if (SLang_Num_Function_Args < 1)
     {
	SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(double[,mode])", fun);
	return;
     }
   if (-1 == get_gsl_mode (&m, SLang_Num_Function_Args-1))
     return;

   slgsl_reset_errors ();
   do_d_dm (f,m);
   slgsl_check_errors (fun);
}

static void do_d_ddm_fun (const char *fun, double (*f)(double, double, gsl_mode_t))
{
   gsl_mode_t m;
   if (SLang_Num_Function_Args < 2)
     {
	SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(double, double [,mode])", fun);
	return;
     }
   if (-1 == get_gsl_mode (&m, SLang_Num_Function_Args-2))
     return;
   slgsl_reset_errors ();
   do_d_ddm (f,m);
   slgsl_check_errors (fun);
}

static void do_d_dddm_fun (const char *fun, double (*f)(double, double, double, gsl_mode_t))
{
   gsl_mode_t m;
   if (SLang_Num_Function_Args < 3)
     {
	SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(double, double, double[,mode])", fun);
	return;
     }
   if (-1 == get_gsl_mode (&m, SLang_Num_Function_Args-3))
     return;
   slgsl_reset_errors ();
   do_d_dddm (f,m);
   slgsl_check_errors (fun);
}

static void do_d_ddddm_fun (const char *fun, double (*f)(double,double,double,double,gsl_mode_t))
{
   gsl_mode_t m;

   if (SLang_Num_Function_Args < 4)
     {
	SLang_verror (SL_USAGE_ERROR, "Usage: y=%s(double, double, double, double [,mode])", fun);
	return;
     }
   if (-1 == get_gsl_mode (&m, SLang_Num_Function_Args-4))
     return;
   slgsl_reset_errors ();
   do_d_ddddm (f,m);
   slgsl_check_errors (fun);
}
#endif				       /* _GSLSF_MODULE_C_ */

/* Macros to aid in wrapping the functions */
#define SLF(f) f##_intrin

#define D_FD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_d_fun (n,f); }
#define D_FDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_dd_fun (n,f); }
#define D_FDDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_ddd_fun (n,f); }
#define D_FDDDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_dddd_fun (n,f); }
#define D_FDM(f,n) \
     static void SLF(f) (void) { do_d_dm_fun (n,f); }
#define D_FDDM(f,n) \
     static void SLF(f) (void) { do_d_ddm_fun (n,f); }
#define D_FDDDM(f,n) \
     static void SLF(f) (void) { do_d_dddm_fun (n,f); }
#define D_FDDDDM(f,n) \
     static void SLF(f) (void) { do_d_ddddm_fun (n,f); }
#define D_FI(f,n) \
     static void SLF(f) (void) { slgsl_do_d_i_fun (n,f); }
#define D_FID(f,n) \
     static void SLF(f) (void) { slgsl_do_d_id_fun (n,f); }
#define D_FIDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_idd_fun (n,f); }
#define D_FIID(f,n) \
     static void SLF(f) (void) { slgsl_do_d_iid_fun (n,f); }
#define D_FIIDD(f,n) \
     static void SLF(f) (void) { slgsl_do_d_iidd_fun (n,f); }
#define I_FD(f,n) \
     static void SLF(f) (void) { slgsl_do_i_d_fun (n,f); }

/*}}}*/

D_FDDDDM(gsl_sf_ellint_RJ,"ellint_RJ")
#if GSL_MAJOR_VERSION >= 2
D_FDDM(gsl_sf_ellint_D,"ellint_D")
#else
D_FDDDM(gsl_sf_ellint_D,"ellint_D")
#endif
D_FDDDM(gsl_sf_ellint_P,"ellint_P")
D_FDDDM(gsl_sf_ellint_RD,"ellint_RD")
D_FDDDM(gsl_sf_ellint_RF,"ellint_RF")
D_FDDM(gsl_sf_ellint_E,"ellint_E")
D_FDDM(gsl_sf_ellint_F,"ellint_F")
D_FDDM(gsl_sf_ellint_Pcomp,"ellint_Pcomp")
D_FDDM(gsl_sf_ellint_RC,"ellint_RC")
D_FDM(gsl_sf_airy_Ai,"airy_Ai")
D_FDM(gsl_sf_airy_Ai_deriv,"airy_Ai_deriv")
D_FDM(gsl_sf_airy_Ai_deriv_scaled,"airy_Ai_deriv_scaled")
D_FDM(gsl_sf_airy_Ai_scaled,"airy_Ai_scaled")
D_FDM(gsl_sf_airy_Bi,"airy_Bi")
D_FDM(gsl_sf_airy_Bi_deriv,"airy_Bi_deriv")
D_FDM(gsl_sf_airy_Bi_deriv_scaled,"airy_Bi_deriv_scaled")
D_FDM(gsl_sf_airy_Bi_scaled,"airy_Bi_scaled")
D_FDM(gsl_sf_ellint_Dcomp,"ellint_Dcomp")
D_FDM(gsl_sf_ellint_Ecomp,"ellint_Ecomp")
D_FDM(gsl_sf_ellint_Kcomp,"ellint_Kcomp")
D_FIIDD(gsl_sf_hydrogenicR,"hydrogenicR")
D_FIIDD(gsl_sf_mathieu_Mc,"mathieu_Mc")
D_FIIDD(gsl_sf_mathieu_Ms,"mathieu_Ms")
D_FIID(gsl_sf_hyperg_1F1_int,"hyperg_1F1_int")
D_FIID(gsl_sf_hyperg_U_int,"hyperg_U_int")
D_FIID(gsl_sf_legendre_Plm,"legendre_Plm")
D_FIID(gsl_sf_legendre_sphPlm,"legendre_sphPlm")
D_FIDD(gsl_sf_conicalP_cyl_reg,"conicalP_cyl_reg")
D_FIDD(gsl_sf_conicalP_sph_reg,"conicalP_sph_reg")
D_FIDD(gsl_sf_gegenpoly_n,"gegenpoly_n")
D_FIDD(gsl_sf_laguerre_n,"laguerre_n")
D_FIDD(gsl_sf_legendre_H3d,"legendre_H3d")
D_FIDD(gsl_sf_mathieu_ce,"mathieu_ce")
D_FIDD(gsl_sf_mathieu_se,"mathieu_se")
D_FID(gsl_sf_bessel_In,"bessel_In")
D_FID(gsl_sf_bessel_In_scaled,"bessel_In_scaled")
D_FID(gsl_sf_bessel_Jn,"bessel_Jn")
D_FID(gsl_sf_bessel_Kn,"bessel_Kn")
D_FID(gsl_sf_bessel_Kn_scaled,"bessel_Kn_scaled")
D_FID(gsl_sf_bessel_Yn,"bessel_Yn")
D_FID(gsl_sf_bessel_il_scaled,"bessel_il_scaled")
D_FID(gsl_sf_bessel_jl,"bessel_jl")
D_FID(gsl_sf_bessel_kl_scaled,"bessel_kl_scaled")
D_FID(gsl_sf_bessel_yl,"bessel_yl")
D_FID(gsl_sf_expint_En,"expint_En")
D_FID(gsl_sf_expint_En_scaled,"expint_En_scaled")
D_FID(gsl_sf_exprel_n,"exprel_n")
D_FID(gsl_sf_fermi_dirac_int,"fermi_dirac_int")
D_FID(gsl_sf_legendre_Pl,"legendre_Pl")
D_FID(gsl_sf_legendre_Ql,"legendre_Ql")
D_FID(gsl_sf_mathieu_a,"mathieu_a")
D_FID(gsl_sf_mathieu_b,"mathieu_b")
D_FID(gsl_sf_psi_n,"psi_n")
D_FID(gsl_sf_taylorcoeff,"taylorcoeff")
D_FI(gsl_sf_eta_int,"eta_int")
D_FI(gsl_sf_psi_1_int,"psi_1_int")
D_FI(gsl_sf_psi_int,"psi_int")
D_FI(gsl_sf_zeta_int,"zeta_int")
D_FI(gsl_sf_zetam1_int,"zetam1_int")
D_FDDDD(gsl_sf_hyperg_2F1,"hyperg_2F1")
D_FDDDD(gsl_sf_hyperg_2F1_conj,"hyperg_2F1_conj")
D_FDDDD(gsl_sf_hyperg_2F1_conj_renorm,"hyperg_2F1_conj_renorm")
D_FDDDD(gsl_sf_hyperg_2F1_renorm,"hyperg_2F1_renorm")
D_FDDD(gsl_sf_beta_inc,"beta_inc")
D_FDDD(gsl_sf_hyperg_1F1,"hyperg_1F1")
D_FDDD(gsl_sf_hyperg_2F0,"hyperg_2F0")
D_FDDD(gsl_sf_hyperg_U,"hyperg_U")
D_FDD(gsl_sf_bessel_Inu,"bessel_Inu")
D_FDD(gsl_sf_bessel_Inu_scaled,"bessel_Inu_scaled")
D_FDD(gsl_sf_bessel_Jnu,"bessel_Jnu")
D_FDD(gsl_sf_bessel_Knu,"bessel_Knu")
D_FDD(gsl_sf_bessel_Knu_scaled,"bessel_Knu_scaled")
D_FDD(gsl_sf_bessel_Ynu,"bessel_Ynu")
D_FDD(gsl_sf_bessel_lnKnu,"bessel_lnKnu")
D_FDD(gsl_sf_beta,"beta")
D_FDD(gsl_sf_conicalP_0,"conicalP_0")
D_FDD(gsl_sf_conicalP_1,"conicalP_1")
D_FDD(gsl_sf_conicalP_half,"conicalP_half")
D_FDD(gsl_sf_conicalP_mhalf,"conicalP_mhalf")
D_FDD(gsl_sf_exp_mult,"exp_mult")
D_FDD(gsl_sf_fermi_dirac_inc_0,"fermi_dirac_inc_0")
D_FDD(gsl_sf_gamma_inc,"gamma_inc")
D_FDD(gsl_sf_gamma_inc_P,"gamma_inc_P")
D_FDD(gsl_sf_gamma_inc_Q,"gamma_inc_Q")
D_FDD(gsl_sf_gegenpoly_1,"gegenpoly_1")
D_FDD(gsl_sf_gegenpoly_2,"gegenpoly_2")
D_FDD(gsl_sf_gegenpoly_3,"gegenpoly_3")
D_FDD(gsl_sf_hydrogenicR_1,"hydrogenicR_1")
D_FDD(gsl_sf_hyperg_0F1,"hyperg_0F1")
D_FDD(gsl_sf_hzeta,"hzeta")
D_FDD(gsl_sf_laguerre_1,"laguerre_1")
D_FDD(gsl_sf_laguerre_2,"laguerre_2")
D_FDD(gsl_sf_laguerre_3,"laguerre_3")
D_FDD(gsl_sf_legendre_H3d_0,"legendre_H3d_0")
D_FDD(gsl_sf_legendre_H3d_1,"legendre_H3d_1")
D_FDD(gsl_sf_lnbeta,"lnbeta")
D_FDD(gsl_sf_lnpoch,"lnpoch")
D_FDD(gsl_sf_poch,"poch")
D_FDD(gsl_sf_pochrel,"pochrel")
D_FD(gsl_sf_Chi,"Chi")
D_FD(gsl_sf_Ci,"Ci")
D_FD(gsl_sf_Shi,"Shi")
D_FD(gsl_sf_Si,"Si")
D_FD(gsl_sf_angle_restrict_pos,"angle_restrict_pos")
D_FD(gsl_sf_angle_restrict_symm,"angle_restrict_symm")
D_FD(gsl_sf_atanint,"atanint")
D_FD(gsl_sf_bessel_I0,"bessel_I0")
D_FD(gsl_sf_bessel_I0_scaled,"bessel_I0_scaled")
D_FD(gsl_sf_bessel_I1,"bessel_I1")
D_FD(gsl_sf_bessel_I1_scaled,"bessel_I1_scaled")
D_FD(gsl_sf_bessel_J0,"bessel_J0")
D_FD(gsl_sf_bessel_J1,"bessel_J1")
D_FD(gsl_sf_bessel_K0,"bessel_K0")
D_FD(gsl_sf_bessel_K0_scaled,"bessel_K0_scaled")
D_FD(gsl_sf_bessel_K1,"bessel_K1")
D_FD(gsl_sf_bessel_K1_scaled,"bessel_K1_scaled")
D_FD(gsl_sf_bessel_Y0,"bessel_Y0")
D_FD(gsl_sf_bessel_Y1,"bessel_Y1")
D_FD(gsl_sf_bessel_i0_scaled,"bessel_i0_scaled")
D_FD(gsl_sf_bessel_i1_scaled,"bessel_i1_scaled")
D_FD(gsl_sf_bessel_i2_scaled,"bessel_i2_scaled")
D_FD(gsl_sf_bessel_j0,"bessel_j0")
D_FD(gsl_sf_bessel_j1,"bessel_j1")
D_FD(gsl_sf_bessel_j2,"bessel_j2")
D_FD(gsl_sf_bessel_k0_scaled,"bessel_k0_scaled")
D_FD(gsl_sf_bessel_k1_scaled,"bessel_k1_scaled")
D_FD(gsl_sf_bessel_k2_scaled,"bessel_k2_scaled")
D_FD(gsl_sf_bessel_y0,"bessel_y0")
D_FD(gsl_sf_bessel_y1,"bessel_y1")
D_FD(gsl_sf_bessel_y2,"bessel_y2")
D_FD(gsl_sf_clausen,"clausen")
D_FD(gsl_sf_dawson,"dawson")
D_FD(gsl_sf_debye_1,"debye_1")
D_FD(gsl_sf_debye_2,"debye_2")
D_FD(gsl_sf_debye_3,"debye_3")
D_FD(gsl_sf_debye_4,"debye_4")
D_FD(gsl_sf_debye_5,"debye_5")
D_FD(gsl_sf_debye_6,"debye_6")
D_FD(gsl_sf_dilog,"dilog")
D_FD(gsl_sf_erf,"erf")
D_FD(gsl_sf_erf_Q,"erf_Q")
D_FD(gsl_sf_erf_Z,"erf_Z")
D_FD(gsl_sf_erfc,"erfc")
D_FD(gsl_sf_eta,"eta")
D_FD(gsl_sf_expint_3,"expint_3")
D_FD(gsl_sf_expint_E1,"expint_E1")
D_FD(gsl_sf_expint_E1_scaled,"expint_E1_scaled")
D_FD(gsl_sf_expint_E2,"expint_E2")
D_FD(gsl_sf_expint_E2_scaled,"expint_E2_scaled")
D_FD(gsl_sf_expint_Ei,"expint_Ei")
D_FD(gsl_sf_expint_Ei_scaled,"expint_Ei_scaled")
D_FD(gsl_sf_expm1,"expm1")
D_FD(gsl_sf_exprel,"exprel")
D_FD(gsl_sf_exprel_2,"exprel_2")
D_FD(gsl_sf_fermi_dirac_0,"fermi_dirac_0")
D_FD(gsl_sf_fermi_dirac_1,"fermi_dirac_1")
D_FD(gsl_sf_fermi_dirac_2,"fermi_dirac_2")
D_FD(gsl_sf_fermi_dirac_3half,"fermi_dirac_3half")
D_FD(gsl_sf_fermi_dirac_half,"fermi_dirac_half")
D_FD(gsl_sf_fermi_dirac_m1,"fermi_dirac_m1")
D_FD(gsl_sf_fermi_dirac_mhalf,"fermi_dirac_mhalf")
D_FD(gsl_sf_gamma,"gamma")
D_FD(gsl_sf_gammainv,"gammainv")
D_FD(gsl_sf_gammastar,"gammastar")
D_FD(gsl_sf_hazard,"hazard")
D_FD(gsl_sf_lambert_W0,"lambert_W0")
D_FD(gsl_sf_lambert_Wm1,"lambert_Wm1")
D_FD(gsl_sf_legendre_P1,"legendre_P1")
D_FD(gsl_sf_legendre_P2,"legendre_P2")
D_FD(gsl_sf_legendre_P3,"legendre_P3")
D_FD(gsl_sf_legendre_Q0,"legendre_Q0")
D_FD(gsl_sf_legendre_Q1,"legendre_Q1")
D_FD(gsl_sf_lncosh,"lncosh")
D_FD(gsl_sf_lngamma,"lngamma")
D_FD(gsl_sf_lnsinh,"lnsinh")
D_FD(gsl_sf_log_1plusx,"log_1plusx")
D_FD(gsl_sf_log_1plusx_mx,"log_1plusx_mx")
D_FD(gsl_sf_log_abs,"log_abs")
D_FD(gsl_sf_log_erfc,"log_erfc")
D_FD(gsl_sf_psi,"psi")
D_FD(gsl_sf_psi_1,"psi_1")
D_FD(gsl_sf_psi_1piy,"psi_1piy")
D_FD(gsl_sf_sinc,"sinc")
D_FD(gsl_sf_synchrotron_1,"synchrotron_1")
D_FD(gsl_sf_synchrotron_2,"synchrotron_2")
D_FD(gsl_sf_transport_2,"transport_2")
D_FD(gsl_sf_transport_3,"transport_3")
D_FD(gsl_sf_transport_4,"transport_4")
D_FD(gsl_sf_transport_5,"transport_5")
D_FD(gsl_sf_zeta,"zeta")
D_FD(gsl_sf_zetam1,"zetam1")

#define V SLANG_VOID_TYPE
static SLang_Intrin_Fun_Type Module_Intrinsics [] =
{
   MAKE_INTRINSIC_0("ellint_RJ", SLF(gsl_sf_ellint_RJ), V),
   MAKE_INTRINSIC_0("ellint_P", SLF(gsl_sf_ellint_P), V),
   MAKE_INTRINSIC_0("ellint_RD", SLF(gsl_sf_ellint_RD), V),
   MAKE_INTRINSIC_0("ellint_RF", SLF(gsl_sf_ellint_RF), V),
   MAKE_INTRINSIC_0("ellint_D", SLF(gsl_sf_ellint_D), V),
   MAKE_INTRINSIC_0("ellint_E", SLF(gsl_sf_ellint_E), V),
   MAKE_INTRINSIC_0("ellint_F", SLF(gsl_sf_ellint_F), V),
   MAKE_INTRINSIC_0("ellint_Pcomp", SLF(gsl_sf_ellint_Pcomp), V),
   MAKE_INTRINSIC_0("ellint_RC", SLF(gsl_sf_ellint_RC), V),
   MAKE_INTRINSIC_0("airy_Ai", SLF(gsl_sf_airy_Ai), V),
   MAKE_INTRINSIC_0("airy_Ai_deriv", SLF(gsl_sf_airy_Ai_deriv), V),
   MAKE_INTRINSIC_0("airy_Ai_deriv_scaled", SLF(gsl_sf_airy_Ai_deriv_scaled), V),
   MAKE_INTRINSIC_0("airy_Ai_scaled", SLF(gsl_sf_airy_Ai_scaled), V),
   MAKE_INTRINSIC_0("airy_Bi", SLF(gsl_sf_airy_Bi), V),
   MAKE_INTRINSIC_0("airy_Bi_deriv", SLF(gsl_sf_airy_Bi_deriv), V),
   MAKE_INTRINSIC_0("airy_Bi_deriv_scaled", SLF(gsl_sf_airy_Bi_deriv_scaled), V),
   MAKE_INTRINSIC_0("airy_Bi_scaled", SLF(gsl_sf_airy_Bi_scaled), V),
   MAKE_INTRINSIC_0("ellint_Dcomp", SLF(gsl_sf_ellint_Dcomp), V),
   MAKE_INTRINSIC_0("ellint_Ecomp", SLF(gsl_sf_ellint_Ecomp), V),
   MAKE_INTRINSIC_0("ellint_Kcomp", SLF(gsl_sf_ellint_Kcomp), V),
   MAKE_INTRINSIC_0("hydrogenicR", SLF(gsl_sf_hydrogenicR), V),
   MAKE_INTRINSIC_0("mathieu_Mc", SLF(gsl_sf_mathieu_Mc), V),
   MAKE_INTRINSIC_0("mathieu_Ms", SLF(gsl_sf_mathieu_Ms), V),
   MAKE_INTRINSIC_0("hyperg_1F1_int", SLF(gsl_sf_hyperg_1F1_int), V),
   MAKE_INTRINSIC_0("hyperg_U_int", SLF(gsl_sf_hyperg_U_int), V),
   MAKE_INTRINSIC_0("legendre_Plm", SLF(gsl_sf_legendre_Plm), V),
   MAKE_INTRINSIC_0("legendre_sphPlm", SLF(gsl_sf_legendre_sphPlm), V),
   MAKE_INTRINSIC_0("conicalP_cyl_reg", SLF(gsl_sf_conicalP_cyl_reg), V),
   MAKE_INTRINSIC_0("conicalP_sph_reg", SLF(gsl_sf_conicalP_sph_reg), V),
   MAKE_INTRINSIC_0("gegenpoly_n", SLF(gsl_sf_gegenpoly_n), V),
   MAKE_INTRINSIC_0("laguerre_n", SLF(gsl_sf_laguerre_n), V),
   MAKE_INTRINSIC_0("legendre_H3d", SLF(gsl_sf_legendre_H3d), V),
   MAKE_INTRINSIC_0("mathieu_ce", SLF(gsl_sf_mathieu_ce), V),
   MAKE_INTRINSIC_0("mathieu_se", SLF(gsl_sf_mathieu_se), V),
   MAKE_INTRINSIC_0("bessel_In", SLF(gsl_sf_bessel_In), V),
   MAKE_INTRINSIC_0("bessel_In_scaled", SLF(gsl_sf_bessel_In_scaled), V),
   MAKE_INTRINSIC_0("bessel_Jn", SLF(gsl_sf_bessel_Jn), V),
   MAKE_INTRINSIC_0("bessel_Kn", SLF(gsl_sf_bessel_Kn), V),
   MAKE_INTRINSIC_0("bessel_Kn_scaled", SLF(gsl_sf_bessel_Kn_scaled), V),
   MAKE_INTRINSIC_0("bessel_Yn", SLF(gsl_sf_bessel_Yn), V),
   MAKE_INTRINSIC_0("bessel_il_scaled", SLF(gsl_sf_bessel_il_scaled), V),
   MAKE_INTRINSIC_0("bessel_jl", SLF(gsl_sf_bessel_jl), V),
   MAKE_INTRINSIC_0("bessel_kl_scaled", SLF(gsl_sf_bessel_kl_scaled), V),
   MAKE_INTRINSIC_0("bessel_yl", SLF(gsl_sf_bessel_yl), V),
   MAKE_INTRINSIC_0("expint_En", SLF(gsl_sf_expint_En), V),
   MAKE_INTRINSIC_0("expint_En_scaled", SLF(gsl_sf_expint_En_scaled), V),
   MAKE_INTRINSIC_0("exprel_n", SLF(gsl_sf_exprel_n), V),
   MAKE_INTRINSIC_0("fermi_dirac_int", SLF(gsl_sf_fermi_dirac_int), V),
   MAKE_INTRINSIC_0("legendre_Pl", SLF(gsl_sf_legendre_Pl), V),
   MAKE_INTRINSIC_0("legendre_Ql", SLF(gsl_sf_legendre_Ql), V),
   MAKE_INTRINSIC_0("mathieu_a", SLF(gsl_sf_mathieu_a), V),
   MAKE_INTRINSIC_0("mathieu_b", SLF(gsl_sf_mathieu_b), V),
   MAKE_INTRINSIC_0("psi_n", SLF(gsl_sf_psi_n), V),
   MAKE_INTRINSIC_0("taylorcoeff", SLF(gsl_sf_taylorcoeff), V),
   MAKE_INTRINSIC_0("eta_int", SLF(gsl_sf_eta_int), V),
   MAKE_INTRINSIC_0("psi_1_int", SLF(gsl_sf_psi_1_int), V),
   MAKE_INTRINSIC_0("psi_int", SLF(gsl_sf_psi_int), V),
   MAKE_INTRINSIC_0("zeta_int", SLF(gsl_sf_zeta_int), V),
   MAKE_INTRINSIC_0("zetam1_int", SLF(gsl_sf_zetam1_int), V),
   MAKE_INTRINSIC_0("hyperg_2F1", SLF(gsl_sf_hyperg_2F1), V),
   MAKE_INTRINSIC_0("hyperg_2F1_conj", SLF(gsl_sf_hyperg_2F1_conj), V),
   MAKE_INTRINSIC_0("hyperg_2F1_conj_renorm", SLF(gsl_sf_hyperg_2F1_conj_renorm), V),
   MAKE_INTRINSIC_0("hyperg_2F1_renorm", SLF(gsl_sf_hyperg_2F1_renorm), V),
   MAKE_INTRINSIC_0("beta_inc", SLF(gsl_sf_beta_inc), V),
   MAKE_INTRINSIC_0("hyperg_1F1", SLF(gsl_sf_hyperg_1F1), V),
   MAKE_INTRINSIC_0("hyperg_2F0", SLF(gsl_sf_hyperg_2F0), V),
   MAKE_INTRINSIC_0("hyperg_U", SLF(gsl_sf_hyperg_U), V),
   MAKE_INTRINSIC_0("bessel_Inu", SLF(gsl_sf_bessel_Inu), V),
   MAKE_INTRINSIC_0("bessel_Inu_scaled", SLF(gsl_sf_bessel_Inu_scaled), V),
   MAKE_INTRINSIC_0("bessel_Jnu", SLF(gsl_sf_bessel_Jnu), V),
   MAKE_INTRINSIC_0("bessel_Knu", SLF(gsl_sf_bessel_Knu), V),
   MAKE_INTRINSIC_0("bessel_Knu_scaled", SLF(gsl_sf_bessel_Knu_scaled), V),
   MAKE_INTRINSIC_0("bessel_Ynu", SLF(gsl_sf_bessel_Ynu), V),
   MAKE_INTRINSIC_0("bessel_lnKnu", SLF(gsl_sf_bessel_lnKnu), V),
   MAKE_INTRINSIC_0("beta", SLF(gsl_sf_beta), V),
   MAKE_INTRINSIC_0("conicalP_0", SLF(gsl_sf_conicalP_0), V),
   MAKE_INTRINSIC_0("conicalP_1", SLF(gsl_sf_conicalP_1), V),
   MAKE_INTRINSIC_0("conicalP_half", SLF(gsl_sf_conicalP_half), V),
   MAKE_INTRINSIC_0("conicalP_mhalf", SLF(gsl_sf_conicalP_mhalf), V),
   MAKE_INTRINSIC_0("exp_mult", SLF(gsl_sf_exp_mult), V),
   MAKE_INTRINSIC_0("fermi_dirac_inc_0", SLF(gsl_sf_fermi_dirac_inc_0), V),
   MAKE_INTRINSIC_0("gamma_inc", SLF(gsl_sf_gamma_inc), V),
   MAKE_INTRINSIC_0("gamma_inc_P", SLF(gsl_sf_gamma_inc_P), V),
   MAKE_INTRINSIC_0("gamma_inc_Q", SLF(gsl_sf_gamma_inc_Q), V),
   MAKE_INTRINSIC_0("gegenpoly_1", SLF(gsl_sf_gegenpoly_1), V),
   MAKE_INTRINSIC_0("gegenpoly_2", SLF(gsl_sf_gegenpoly_2), V),
   MAKE_INTRINSIC_0("gegenpoly_3", SLF(gsl_sf_gegenpoly_3), V),
   MAKE_INTRINSIC_0("hydrogenicR_1", SLF(gsl_sf_hydrogenicR_1), V),
   MAKE_INTRINSIC_0("hyperg_0F1", SLF(gsl_sf_hyperg_0F1), V),
   MAKE_INTRINSIC_0("hzeta", SLF(gsl_sf_hzeta), V),
   MAKE_INTRINSIC_0("laguerre_1", SLF(gsl_sf_laguerre_1), V),
   MAKE_INTRINSIC_0("laguerre_2", SLF(gsl_sf_laguerre_2), V),
   MAKE_INTRINSIC_0("laguerre_3", SLF(gsl_sf_laguerre_3), V),
   MAKE_INTRINSIC_0("legendre_H3d_0", SLF(gsl_sf_legendre_H3d_0), V),
   MAKE_INTRINSIC_0("legendre_H3d_1", SLF(gsl_sf_legendre_H3d_1), V),
   MAKE_INTRINSIC_0("lnbeta", SLF(gsl_sf_lnbeta), V),
   MAKE_INTRINSIC_0("lnpoch", SLF(gsl_sf_lnpoch), V),
   MAKE_INTRINSIC_0("poch", SLF(gsl_sf_poch), V),
   MAKE_INTRINSIC_0("pochrel", SLF(gsl_sf_pochrel), V),
   MAKE_INTRINSIC_0("Chi", SLF(gsl_sf_Chi), V),
   MAKE_INTRINSIC_0("Ci", SLF(gsl_sf_Ci), V),
   MAKE_INTRINSIC_0("Shi", SLF(gsl_sf_Shi), V),
   MAKE_INTRINSIC_0("Si", SLF(gsl_sf_Si), V),
   MAKE_INTRINSIC_0("angle_restrict_pos", SLF(gsl_sf_angle_restrict_pos), V),
   MAKE_INTRINSIC_0("angle_restrict_symm", SLF(gsl_sf_angle_restrict_symm), V),
   MAKE_INTRINSIC_0("atanint", SLF(gsl_sf_atanint), V),
   MAKE_INTRINSIC_0("bessel_I0", SLF(gsl_sf_bessel_I0), V),
   MAKE_INTRINSIC_0("bessel_I0_scaled", SLF(gsl_sf_bessel_I0_scaled), V),
   MAKE_INTRINSIC_0("bessel_I1", SLF(gsl_sf_bessel_I1), V),
   MAKE_INTRINSIC_0("bessel_I1_scaled", SLF(gsl_sf_bessel_I1_scaled), V),
   MAKE_INTRINSIC_0("bessel_J0", SLF(gsl_sf_bessel_J0), V),
   MAKE_INTRINSIC_0("bessel_J1", SLF(gsl_sf_bessel_J1), V),
   MAKE_INTRINSIC_0("bessel_K0", SLF(gsl_sf_bessel_K0), V),
   MAKE_INTRINSIC_0("bessel_K0_scaled", SLF(gsl_sf_bessel_K0_scaled), V),
   MAKE_INTRINSIC_0("bessel_K1", SLF(gsl_sf_bessel_K1), V),
   MAKE_INTRINSIC_0("bessel_K1_scaled", SLF(gsl_sf_bessel_K1_scaled), V),
   MAKE_INTRINSIC_0("bessel_Y0", SLF(gsl_sf_bessel_Y0), V),
   MAKE_INTRINSIC_0("bessel_Y1", SLF(gsl_sf_bessel_Y1), V),
   MAKE_INTRINSIC_0("bessel_i0_scaled", SLF(gsl_sf_bessel_i0_scaled), V),
   MAKE_INTRINSIC_0("bessel_i1_scaled", SLF(gsl_sf_bessel_i1_scaled), V),
   MAKE_INTRINSIC_0("bessel_i2_scaled", SLF(gsl_sf_bessel_i2_scaled), V),
   MAKE_INTRINSIC_0("bessel_j0", SLF(gsl_sf_bessel_j0), V),
   MAKE_INTRINSIC_0("bessel_j1", SLF(gsl_sf_bessel_j1), V),
   MAKE_INTRINSIC_0("bessel_j2", SLF(gsl_sf_bessel_j2), V),
   MAKE_INTRINSIC_0("bessel_k0_scaled", SLF(gsl_sf_bessel_k0_scaled), V),
   MAKE_INTRINSIC_0("bessel_k1_scaled", SLF(gsl_sf_bessel_k1_scaled), V),
   MAKE_INTRINSIC_0("bessel_k2_scaled", SLF(gsl_sf_bessel_k2_scaled), V),
   MAKE_INTRINSIC_0("bessel_y0", SLF(gsl_sf_bessel_y0), V),
   MAKE_INTRINSIC_0("bessel_y1", SLF(gsl_sf_bessel_y1), V),
   MAKE_INTRINSIC_0("bessel_y2", SLF(gsl_sf_bessel_y2), V),
   MAKE_INTRINSIC_0("clausen", SLF(gsl_sf_clausen), V),
   MAKE_INTRINSIC_0("dawson", SLF(gsl_sf_dawson), V),
   MAKE_INTRINSIC_0("debye_1", SLF(gsl_sf_debye_1), V),
   MAKE_INTRINSIC_0("debye_2", SLF(gsl_sf_debye_2), V),
   MAKE_INTRINSIC_0("debye_3", SLF(gsl_sf_debye_3), V),
   MAKE_INTRINSIC_0("debye_4", SLF(gsl_sf_debye_4), V),
   MAKE_INTRINSIC_0("debye_5", SLF(gsl_sf_debye_5), V),
   MAKE_INTRINSIC_0("debye_6", SLF(gsl_sf_debye_6), V),
   MAKE_INTRINSIC_0("dilog", SLF(gsl_sf_dilog), V),
   MAKE_INTRINSIC_0("erf", SLF(gsl_sf_erf), V),
   MAKE_INTRINSIC_0("erf_Q", SLF(gsl_sf_erf_Q), V),
   MAKE_INTRINSIC_0("erf_Z", SLF(gsl_sf_erf_Z), V),
   MAKE_INTRINSIC_0("erfc", SLF(gsl_sf_erfc), V),
   MAKE_INTRINSIC_0("eta", SLF(gsl_sf_eta), V),
   MAKE_INTRINSIC_0("expint_3", SLF(gsl_sf_expint_3), V),
   MAKE_INTRINSIC_0("expint_E1", SLF(gsl_sf_expint_E1), V),
   MAKE_INTRINSIC_0("expint_E1_scaled", SLF(gsl_sf_expint_E1_scaled), V),
   MAKE_INTRINSIC_0("expint_E2", SLF(gsl_sf_expint_E2), V),
   MAKE_INTRINSIC_0("expint_E2_scaled", SLF(gsl_sf_expint_E2_scaled), V),
   MAKE_INTRINSIC_0("expint_Ei", SLF(gsl_sf_expint_Ei), V),
   MAKE_INTRINSIC_0("expint_Ei_scaled", SLF(gsl_sf_expint_Ei_scaled), V),
   MAKE_INTRINSIC_0("expm1", SLF(gsl_sf_expm1), V),
   MAKE_INTRINSIC_0("exprel", SLF(gsl_sf_exprel), V),
   MAKE_INTRINSIC_0("exprel_2", SLF(gsl_sf_exprel_2), V),
   MAKE_INTRINSIC_0("fermi_dirac_0", SLF(gsl_sf_fermi_dirac_0), V),
   MAKE_INTRINSIC_0("fermi_dirac_1", SLF(gsl_sf_fermi_dirac_1), V),
   MAKE_INTRINSIC_0("fermi_dirac_2", SLF(gsl_sf_fermi_dirac_2), V),
   MAKE_INTRINSIC_0("fermi_dirac_3half", SLF(gsl_sf_fermi_dirac_3half), V),
   MAKE_INTRINSIC_0("fermi_dirac_half", SLF(gsl_sf_fermi_dirac_half), V),
   MAKE_INTRINSIC_0("fermi_dirac_m1", SLF(gsl_sf_fermi_dirac_m1), V),
   MAKE_INTRINSIC_0("fermi_dirac_mhalf", SLF(gsl_sf_fermi_dirac_mhalf), V),
   MAKE_INTRINSIC_0("gamma", SLF(gsl_sf_gamma), V),
   MAKE_INTRINSIC_0("gammainv", SLF(gsl_sf_gammainv), V),
   MAKE_INTRINSIC_0("gammastar", SLF(gsl_sf_gammastar), V),
   MAKE_INTRINSIC_0("hazard", SLF(gsl_sf_hazard), V),
   MAKE_INTRINSIC_0("lambert_W0", SLF(gsl_sf_lambert_W0), V),
   MAKE_INTRINSIC_0("lambert_Wm1", SLF(gsl_sf_lambert_Wm1), V),
   MAKE_INTRINSIC_0("legendre_P1", SLF(gsl_sf_legendre_P1), V),
   MAKE_INTRINSIC_0("legendre_P2", SLF(gsl_sf_legendre_P2), V),
   MAKE_INTRINSIC_0("legendre_P3", SLF(gsl_sf_legendre_P3), V),
   MAKE_INTRINSIC_0("legendre_Q0", SLF(gsl_sf_legendre_Q0), V),
   MAKE_INTRINSIC_0("legendre_Q1", SLF(gsl_sf_legendre_Q1), V),
   MAKE_INTRINSIC_0("lncosh", SLF(gsl_sf_lncosh), V),
   MAKE_INTRINSIC_0("lngamma", SLF(gsl_sf_lngamma), V),
   MAKE_INTRINSIC_0("lnsinh", SLF(gsl_sf_lnsinh), V),
   MAKE_INTRINSIC_0("log_1plusx", SLF(gsl_sf_log_1plusx), V),
   MAKE_INTRINSIC_0("log_1plusx_mx", SLF(gsl_sf_log_1plusx_mx), V),
   MAKE_INTRINSIC_0("log_abs", SLF(gsl_sf_log_abs), V),
   MAKE_INTRINSIC_0("log_erfc", SLF(gsl_sf_log_erfc), V),
   MAKE_INTRINSIC_0("psi", SLF(gsl_sf_psi), V),
   MAKE_INTRINSIC_0("psi_1", SLF(gsl_sf_psi_1), V),
   MAKE_INTRINSIC_0("psi_1piy", SLF(gsl_sf_psi_1piy), V),
   MAKE_INTRINSIC_0("sinc", SLF(gsl_sf_sinc), V),
   MAKE_INTRINSIC_0("synchrotron_1", SLF(gsl_sf_synchrotron_1), V),
   MAKE_INTRINSIC_0("synchrotron_2", SLF(gsl_sf_synchrotron_2), V),
   MAKE_INTRINSIC_0("transport_2", SLF(gsl_sf_transport_2), V),
   MAKE_INTRINSIC_0("transport_3", SLF(gsl_sf_transport_3), V),
   MAKE_INTRINSIC_0("transport_4", SLF(gsl_sf_transport_4), V),
   MAKE_INTRINSIC_0("transport_5", SLF(gsl_sf_transport_5), V),
   MAKE_INTRINSIC_0("zeta", SLF(gsl_sf_zeta), V),
   MAKE_INTRINSIC_0("zetam1", SLF(gsl_sf_zetam1), V),
#ifdef _GSLSF_MODULE_C_
   MAKE_INTRINSIC_0("gslsf_get_precision", get_gsl_precision, SLANG_INT_TYPE),
   MAKE_INTRINSIC_I("gslsf_set_precision", set_gsl_precision, SLANG_VOID_TYPE),
#endif
   SLANG_END_INTRIN_FUN_TABLE
};
#undef V
#endif				       /* MODULE_HAS_INTRINSICS */

static SLang_Intrin_Var_Type Module_Variables [] =
{
   MAKE_VARIABLE("_gslsf_module_version_string", &Module_Version_String, SLANG_STRING_TYPE, 1),
   MAKE_VARIABLE("GSL_VERSION", &gsl_version, SLANG_STRING_TYPE, 1),
   SLANG_END_INTRIN_VAR_TABLE
};

static SLang_IConstant_Type Module_IConstants [] =
{
   MAKE_ICONSTANT("_gslsf_module_version", MODULE_VERSION_NUMBER),
#ifdef _GSLSF_MODULE_C_
   MAKE_ICONSTANT("GSL_PREC_SINGLE", GSL_PREC_SINGLE),
   MAKE_ICONSTANT("GSL_PREC_DOUBLE", GSL_PREC_DOUBLE),
   MAKE_ICONSTANT("GSL_PREC_APPROX", GSL_PREC_APPROX),
#endif
   SLANG_END_ICONST_TABLE
};

#ifdef MODULE_HAS_DCONSTANTS
static SLang_DConstant_Type Module_DConstants [] =
{
   SLANG_END_DCONST_TABLE
};
#endif

int init_gslsf_module_ns (char *ns_name)
{
   SLang_NameSpace_Type *ns = SLns_create_namespace (ns_name);
   if (ns == NULL)
     return -1;

   if (
       (-1 == SLns_add_intrin_var_table (ns, Module_Variables, NULL))
#ifdef MODULE_HAS_INTRINSICS
       || (-1 == SLns_add_intrin_fun_table (ns, Module_Intrinsics, NULL))
#endif
       || (-1 == SLns_add_iconstant_table (ns, Module_IConstants, NULL))
#ifdef MODULE_HAS_DCONSTANTS
       || (-1 == SLns_add_dconstant_table (ns, Module_DConstants, NULL))
#endif
       )
     return -1;

   return 0;
}

/* This function is optional */
void deinit_gslsf_module (void)
{
}
