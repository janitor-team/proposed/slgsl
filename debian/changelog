slgsl (0.10.0~pre.5-2) unstable; urgency=medium

  * Upload to unstable

 -- Rafael Laboissière <rafael@debian.org>  Sat, 11 Dec 2021 11:48:41 -0300

slgsl (0.10.0~pre.5-1) experimental; urgency=medium

  * Upload to experimental (do not interfere with the libgsl 25->26
    transition)

  * New upstream version 0.10.0~pre.5
  * Adopt package (Closes: #748262)
  * d/control: Add Multi-Arch: same
  * d/slang-gsl.examples: Install unit tests as examples
  * d/t/control: Use make in test command
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Thu, 02 Dec 2021 04:04:31 -0300

slgsl (0.10.0~pre.4-2) unstable; urgency=medium

  * QA upload.

  * d/control: Drop versioned constraint on jed and slrn in Suggests
  * d/t/control: Run unit tests without using the Makefile

 -- Rafael Laboissière <rafael@debian.org>  Thu, 30 Sep 2021 06:06:29 -0300

slgsl (0.10.0~pre.4-1) unstable; urgency=medium

  * QA upload.

  * New upstream version 0.10.0~pre.4
  * Create the Git repository at Salsa.debian.org
  * Bump debhelper compatibility level to = 13
    + d/control: Build-depend on debhelper-compat (= 13)
    + d/compat: Drop file
    + d/rules: Use option --no-parallel in dh sequencer
  * d/control:
    + Bump Standards-Version to 4.6.0 (no changes needed)
    + Add Rules-Requires-Root: no
  * d/copyright: Convert to DEP-5 format
  * Activate dh_autoreconf (Closes: #765254)
    + d/rules: Move the generated autoconf/configure to the top directory
    + d/autoreconf: New file
    + d/p/update-configure.patch: New patch
    + d/source/options: Ignore the configure file
    + d/clean: Remove the leftover directory autoconf/autoconf/
  * Exercise the unit tests
    + d/rules: Override dh_auto_test and set the correct S-Lang load path
    + d/p/slsh-path-unit-tests.patch
  * d/watch: Consider the upstream pre-release tarballs
  * d/t/control: Add autopkgtest support
  * Drop patches (the problems are fixed upstream)
    + d/p/find_gsl.diff
    + d/p/use-fPIC.patch

 -- Rafael Laboissière <rafael@debian.org>  Mon, 27 Sep 2021 15:15:45 -0300

slgsl (0.7.0-6) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group.  (See: #748262) (Closes: #571890)
  * Update Build-Depends for GSL 2, change libgsl0-dev to libgsl-dev, thanks
    to Bas Couwenberg.  (Closes: #807227)
  * Use source format 3.0 (quilt).
  * Use debhelper compat level 9.
  * Switch from cdbs to dh.
  * Move git repository to collab-maint.
  * Bump Standards-Version to 3.9.6, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 16 Jan 2016 16:02:21 +0100

slgsl (0.7.0-5.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS due to detect gsl error. (Closes: #796402)

 -- YunQiang Su <syq@debian.org>  Mon, 16 Nov 2015 20:40:25 +0800

slgsl (0.7.0-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS: use --with-slanglib multiarch path (Closes: #640685)

 -- Simon Paillard <spaillard@debian.Org>  Sat, 17 Dec 2011 10:30:39 +0100

slgsl (0.7.0-5) unstable; urgency=low

  * debian/control:
    + Bump Standards-Version to 3.8.1 (add debian/README.source file telling
      that quilt is used to obtain the upstream sources patched for Debian)
    + The package is now maintained with Git at alioth.debian.org.  Add
      Vcs-Git field and change Vcs-Browser accordingly.
    + Fix Lintian warning debhelper-but-no-misc-depends
    + Bump build dependency on debhelper to >= 7
  * debian/compat: Set compatibility level of debhelper to 7
  * debian/patches/use-fPIC.patch: Add description (fix Lintian warning)
  * debian/watch: Add file for uscan

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 26 Apr 2009 13:02:08 +0200

slgsl (0.7.0-4) unstable; urgency=low

  * Switch from CDBS' simple patchsys to quilt
  * debian/control: Fix spelling of S-Lang in description

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 17 Mar 2008 16:47:16 +0100

slgsl (0.7.0-3) unstable; urgency=low

  * debian/control:
    + Use the now official Vcs-* fields instead of the obsolete XS-Vcs-*
    + Dropped the Homepage pseudo-header from the extended description
    + Bumped Standards-Version to 3.7.3

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 15 Dec 2007 17:40:24 +0100

slgsl (0.7.0-2) unstable; urgency=low

  * debian/patches/use-fPIC.patch: Added patch for compiling files with
    -fPIC (closes: #444316)

 -- Rafael Laboissiere <rafael@debian.org>  Thu, 27 Sep 2007 23:45:23 +0200

slgsl (0.7.0-1) unstable; urgency=low

  * New upstream release
  * debian/control: Added Homepage field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 21 Sep 2007 17:23:43 +0200

slgsl (0.5.4-1) unstable; urgency=low

  * New upstream release

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 18 Dec 2006 11:15:28 +0100

slgsl (0.5.2-5) unstable; urgency=low

  * debian/control:
    - Added my name to the Uploaders list
    - Bumped Standards-Version to 3.7.2 (no changes needed)

 -- Rafael Laboissiere <rafael@debian.org>  Tue, 15 Aug 2006 23:27:44 +0200

slgsl (0.5.2-4) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * debian/watch: Added file

 -- Debian JED Group <pkg-jed-sl-modules@lists.alioth.debian.org>  Fri, 11 Nov 2005 00:30:12 +0100

slgsl (0.5.2-3) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * debian/control: Changed the maintainer to
    pkg-jed-sl-modules@lists.alioth.debian.org

 -- Debian JED Group <pkg-jed-sl-modules@lists.alioth.debian.org>  Tue,  8 Nov 2005 23:06:25 +0100

slgsl (0.5.2-2) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * debian/control: Build-Depend on slsh (closes: #337980)

 -- Debian JED Group <pkg-jed-devel@lists.alioth.debian.org>  Mon,  7 Nov 2005 21:56:19 +0100

slgsl (0.5.2-1) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * First official release  (closes: #336255)

 -- Debian JED Group <pkg-jed-devel@lists.alioth.debian.org>  Sat, 29 Oct 2005 18:26:09 +0200

slgsl (0.5.2-0.1) unstable; urgency=low

   +++ Changes by Rafael Laboissiere

  * Initial release

 -- Debian JED Group <pkg-jed-devel@lists.alioth.debian.org>  Wed, 26 Oct 2005 15:43:11 +0200

Local Variables:
eval: (add-local-hook
  'debian-changelog-add-version-hook
  (lambda ()
    (save-excursion
      (forward-line -1)
      (beginning-of-line)
      (insert "\n  [ "
        (or (getenv "DEBFULLNAME") (user-full-name)) " ]"))))
End:
