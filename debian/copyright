Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: the S-Lang gsl module
Upstream-Contact: John E. Davis <davis@space.mit.edu>
Source: https://space.mit.edu/cxc/software/slang/modules/gsl/

Files: *
Copyright: 2003-2021 Massachusetts Institute of Technology
License: MIT
 Permission to use, copy, modify, distribute, and sell this software
 and its documentation for any purpose is hereby granted without fee,
 provided that the above copyright notice appear in all copies and
 that both that copyright notice and this permission notice appear in
 the supporting documentation, and that the name of the Massachusetts
 Institute of Technology not be used in advertising or publicity
 pertaining to distribution of the software without specific, written
 prior permission.  The Massachusetts Institute of Technology makes
 no representations about the suitability of this software for any
 purpose.  It is provided "as is" without express or implied warranty.
 .
 THE MASSACHUSETTS INSTITUTE OF TECHNOLOGY DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL THE MASSACHUSETTS
 INSTITUTE OF TECHNOLOGY BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: debian/*
Copyright: 2005-2009, 2021 Rafael Laboissière <rafael@debian.org>
           2011 Simon Paillard <spaillard@debian.org>
           2015 YunQiang Su <syq@debian.org>
           2016 Andreas Beckmann <anbe@debian.org>
License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.
