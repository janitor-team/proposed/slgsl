\function_sect{CDF Functions}
\function{cdf_beta_P}
\synopsis{S-Lang version of gsl_cdf_beta_P}
\usage{Double_Type[] cdf_beta_P (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_beta_Pinv}
\synopsis{S-Lang version of gsl_cdf_beta_Pinv}
\usage{Double_Type[] cdf_beta_Pinv (P, a, b)}
#v+
  Double_Type[] P
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_beta_Q}
\synopsis{S-Lang version of gsl_cdf_beta_Q}
\usage{Double_Type[] cdf_beta_Q (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_beta_Qinv}
\synopsis{S-Lang version of gsl_cdf_beta_Qinv}
\usage{Double_Type[] cdf_beta_Qinv (Q, a, b)}
#v+
  Double_Type[] Q
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_cauchy_P}
\synopsis{S-Lang version of gsl_cdf_cauchy_P}
\usage{Double_Type[] cdf_cauchy_P (Double_Type[] x, Double_Type[] a)}
\done

\function{cdf_cauchy_Pinv}
\synopsis{S-Lang version of gsl_cdf_cauchy_Pinv}
\usage{Double_Type[] cdf_cauchy_Pinv (Double_Type[] P, Double_Type[] a)}
\done

\function{cdf_cauchy_Q}
\synopsis{S-Lang version of gsl_cdf_cauchy_Q}
\usage{Double_Type[] cdf_cauchy_Q (Double_Type[] x, Double_Type[] a)}
\done

\function{cdf_cauchy_Qinv}
\synopsis{S-Lang version of gsl_cdf_cauchy_Qinv}
\usage{Double_Type[] cdf_cauchy_Qinv (Double_Type[] Q, Double_Type[] a)}
\done

\function{cdf_chisq_P}
\synopsis{S-Lang version of gsl_cdf_chisq_P}
\usage{Double_Type[] cdf_chisq_P (Double_Type[] x, Double_Type[] nu)}
\done

\function{cdf_chisq_Pinv}
\synopsis{S-Lang version of gsl_cdf_chisq_Pinv}
\usage{Double_Type[] cdf_chisq_Pinv (Double_Type[] P, Double_Type[] nu)}
\done

\function{cdf_chisq_Q}
\synopsis{S-Lang version of gsl_cdf_chisq_Q}
\usage{Double_Type[] cdf_chisq_Q (Double_Type[] x, Double_Type[] nu)}
\done

\function{cdf_chisq_Qinv}
\synopsis{S-Lang version of gsl_cdf_chisq_Qinv}
\usage{Double_Type[] cdf_chisq_Qinv (Double_Type[] Q, Double_Type[] nu)}
\done

\function{cdf_exponential_P}
\synopsis{S-Lang version of gsl_cdf_exponential_P}
\usage{Double_Type[] cdf_exponential_P (Double_Type[] x, Double_Type[] mu)}
\done

\function{cdf_exponential_Pinv}
\synopsis{S-Lang version of gsl_cdf_exponential_Pinv}
\usage{Double_Type[] cdf_exponential_Pinv (Double_Type[] P, Double_Type[] mu)}
\done

\function{cdf_exponential_Q}
\synopsis{S-Lang version of gsl_cdf_exponential_Q}
\usage{Double_Type[] cdf_exponential_Q (Double_Type[] x, Double_Type[] mu)}
\done

\function{cdf_exponential_Qinv}
\synopsis{S-Lang version of gsl_cdf_exponential_Qinv}
\usage{Double_Type[] cdf_exponential_Qinv (Double_Type[] Q, Double_Type[] mu)}
\done

\function{cdf_exppow_P}
\synopsis{S-Lang version of gsl_cdf_exppow_P}
\usage{Double_Type[] cdf_exppow_P (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_exppow_Q}
\synopsis{S-Lang version of gsl_cdf_exppow_Q}
\usage{Double_Type[] cdf_exppow_Q (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_fdist_P}
\synopsis{S-Lang version of gsl_cdf_fdist_P}
\usage{Double_Type[] cdf_fdist_P (x, nu1, nu2)}
#v+
  Double_Type[] x
  Double_Type[] nu1
  Double_Type[] nu2
#v-
\done

\function{cdf_fdist_Pinv}
\synopsis{S-Lang version of gsl_cdf_fdist_Pinv}
\usage{Double_Type[] cdf_fdist_Pinv (P, nu1, nu2)}
#v+
  Double_Type[] P
  Double_Type[] nu1
  Double_Type[] nu2
#v-
\done

\function{cdf_fdist_Q}
\synopsis{S-Lang version of gsl_cdf_fdist_Q}
\usage{Double_Type[] cdf_fdist_Q (x, nu1, nu2)}
#v+
  Double_Type[] x
  Double_Type[] nu1
  Double_Type[] nu2
#v-
\done

\function{cdf_fdist_Qinv}
\synopsis{S-Lang version of gsl_cdf_fdist_Qinv}
\usage{Double_Type[] cdf_fdist_Qinv (Q, nu1, nu2)}
#v+
  Double_Type[] Q
  Double_Type[] nu1
  Double_Type[] nu2
#v-
\done

\function{cdf_flat_P}
\synopsis{S-Lang version of gsl_cdf_flat_P}
\usage{Double_Type[] cdf_flat_P (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_flat_Pinv}
\synopsis{S-Lang version of gsl_cdf_flat_Pinv}
\usage{Double_Type[] cdf_flat_Pinv (P, a, b)}
#v+
  Double_Type[] P
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_flat_Q}
\synopsis{S-Lang version of gsl_cdf_flat_Q}
\usage{Double_Type[] cdf_flat_Q (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_flat_Qinv}
\synopsis{S-Lang version of gsl_cdf_flat_Qinv}
\usage{Double_Type[] cdf_flat_Qinv (Q, a, b)}
#v+
  Double_Type[] Q
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gamma_P}
\synopsis{S-Lang version of gsl_cdf_gamma_P}
\usage{Double_Type[] cdf_gamma_P (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gamma_Pinv}
\synopsis{S-Lang version of gsl_cdf_gamma_Pinv}
\usage{Double_Type[] cdf_gamma_Pinv (P, a, b)}
#v+
  Double_Type[] P
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gamma_Q}
\synopsis{S-Lang version of gsl_cdf_gamma_Q}
\usage{Double_Type[] cdf_gamma_Q (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gamma_Qinv}
\synopsis{S-Lang version of gsl_cdf_gamma_Qinv}
\usage{Double_Type[] cdf_gamma_Qinv (Q, a, b)}
#v+
  Double_Type[] Q
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gaussian_P}
\synopsis{S-Lang version of gsl_cdf_gaussian_P}
\usage{Double_Type[] cdf_gaussian_P (Double_Type[] x, Double_Type[] sigma)}
\done

\function{cdf_gaussian_Pinv}
\synopsis{S-Lang version of gsl_cdf_gaussian_Pinv}
\usage{Double_Type[] cdf_gaussian_Pinv (Double_Type[] P, Double_Type[] sigma)}
\done

\function{cdf_gaussian_Q}
\synopsis{S-Lang version of gsl_cdf_gaussian_Q}
\usage{Double_Type[] cdf_gaussian_Q (Double_Type[] x, Double_Type[] sigma)}
\done

\function{cdf_gaussian_Qinv}
\synopsis{S-Lang version of gsl_cdf_gaussian_Qinv}
\usage{Double_Type[] cdf_gaussian_Qinv (Double_Type[] Q, Double_Type[] sigma)}
\done

\function{cdf_gumbel1_P}
\synopsis{S-Lang version of gsl_cdf_gumbel1_P}
\usage{Double_Type[] cdf_gumbel1_P (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gumbel1_Pinv}
\synopsis{S-Lang version of gsl_cdf_gumbel1_Pinv}
\usage{Double_Type[] cdf_gumbel1_Pinv (P, a, b)}
#v+
  Double_Type[] P
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gumbel1_Q}
\synopsis{S-Lang version of gsl_cdf_gumbel1_Q}
\usage{Double_Type[] cdf_gumbel1_Q (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gumbel1_Qinv}
\synopsis{S-Lang version of gsl_cdf_gumbel1_Qinv}
\usage{Double_Type[] cdf_gumbel1_Qinv (Q, a, b)}
#v+
  Double_Type[] Q
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gumbel2_P}
\synopsis{S-Lang version of gsl_cdf_gumbel2_P}
\usage{Double_Type[] cdf_gumbel2_P (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gumbel2_Pinv}
\synopsis{S-Lang version of gsl_cdf_gumbel2_Pinv}
\usage{Double_Type[] cdf_gumbel2_Pinv (P, a, b)}
#v+
  Double_Type[] P
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gumbel2_Q}
\synopsis{S-Lang version of gsl_cdf_gumbel2_Q}
\usage{Double_Type[] cdf_gumbel2_Q (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_gumbel2_Qinv}
\synopsis{S-Lang version of gsl_cdf_gumbel2_Qinv}
\usage{Double_Type[] cdf_gumbel2_Qinv (Q, a, b)}
#v+
  Double_Type[] Q
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_laplace_P}
\synopsis{S-Lang version of gsl_cdf_laplace_P}
\usage{Double_Type[] cdf_laplace_P (Double_Type[] x, Double_Type[] a)}
\done

\function{cdf_laplace_Pinv}
\synopsis{S-Lang version of gsl_cdf_laplace_Pinv}
\usage{Double_Type[] cdf_laplace_Pinv (Double_Type[] P, Double_Type[] a)}
\done

\function{cdf_laplace_Q}
\synopsis{S-Lang version of gsl_cdf_laplace_Q}
\usage{Double_Type[] cdf_laplace_Q (Double_Type[] x, Double_Type[] a)}
\done

\function{cdf_laplace_Qinv}
\synopsis{S-Lang version of gsl_cdf_laplace_Qinv}
\usage{Double_Type[] cdf_laplace_Qinv (Double_Type[] Q, Double_Type[] a)}
\done

\function{cdf_logistic_P}
\synopsis{S-Lang version of gsl_cdf_logistic_P}
\usage{Double_Type[] cdf_logistic_P (Double_Type[] x, Double_Type[] a)}
\done

\function{cdf_logistic_Pinv}
\synopsis{S-Lang version of gsl_cdf_logistic_Pinv}
\usage{Double_Type[] cdf_logistic_Pinv (Double_Type[] P, Double_Type[] a)}
\done

\function{cdf_logistic_Q}
\synopsis{S-Lang version of gsl_cdf_logistic_Q}
\usage{Double_Type[] cdf_logistic_Q (Double_Type[] x, Double_Type[] a)}
\done

\function{cdf_logistic_Qinv}
\synopsis{S-Lang version of gsl_cdf_logistic_Qinv}
\usage{Double_Type[] cdf_logistic_Qinv (Double_Type[] Q, Double_Type[] a)}
\done

\function{cdf_lognormal_P}
\synopsis{S-Lang version of gsl_cdf_lognormal_P}
\usage{Double_Type[] cdf_lognormal_P (x, zeta, sigma)}
#v+
  Double_Type[] x
  Double_Type[] zeta
  Double_Type[] sigma
#v-
\done

\function{cdf_lognormal_Pinv}
\synopsis{S-Lang version of gsl_cdf_lognormal_Pinv}
\usage{Double_Type[] cdf_lognormal_Pinv (P, zeta, sigma)}
#v+
  Double_Type[] P
  Double_Type[] zeta
  Double_Type[] sigma
#v-
\done

\function{cdf_lognormal_Q}
\synopsis{S-Lang version of gsl_cdf_lognormal_Q}
\usage{Double_Type[] cdf_lognormal_Q (x, zeta, sigma)}
#v+
  Double_Type[] x
  Double_Type[] zeta
  Double_Type[] sigma
#v-
\done

\function{cdf_lognormal_Qinv}
\synopsis{S-Lang version of gsl_cdf_lognormal_Qinv}
\usage{Double_Type[] cdf_lognormal_Qinv (Q, zeta, sigma)}
#v+
  Double_Type[] Q
  Double_Type[] zeta
  Double_Type[] sigma
#v-
\done

\function{cdf_pareto_P}
\synopsis{S-Lang version of gsl_cdf_pareto_P}
\usage{Double_Type[] cdf_pareto_P (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_pareto_Pinv}
\synopsis{S-Lang version of gsl_cdf_pareto_Pinv}
\usage{Double_Type[] cdf_pareto_Pinv (P, a, b)}
#v+
  Double_Type[] P
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_pareto_Q}
\synopsis{S-Lang version of gsl_cdf_pareto_Q}
\usage{Double_Type[] cdf_pareto_Q (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_pareto_Qinv}
\synopsis{S-Lang version of gsl_cdf_pareto_Qinv}
\usage{Double_Type[] cdf_pareto_Qinv (Q, a, b)}
#v+
  Double_Type[] Q
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_rayleigh_P}
\synopsis{S-Lang version of gsl_cdf_rayleigh_P}
\usage{Double_Type[] cdf_rayleigh_P (Double_Type[] x, Double_Type[] sigma)}
\done

\function{cdf_rayleigh_Pinv}
\synopsis{S-Lang version of gsl_cdf_rayleigh_Pinv}
\usage{Double_Type[] cdf_rayleigh_Pinv (Double_Type[] P, Double_Type[] sigma)}
\done

\function{cdf_rayleigh_Q}
\synopsis{S-Lang version of gsl_cdf_rayleigh_Q}
\usage{Double_Type[] cdf_rayleigh_Q (Double_Type[] x, Double_Type[] sigma)}
\done

\function{cdf_rayleigh_Qinv}
\synopsis{S-Lang version of gsl_cdf_rayleigh_Qinv}
\usage{Double_Type[] cdf_rayleigh_Qinv (Double_Type[] Q, Double_Type[] sigma)}
\done

\function{cdf_tdist_P}
\synopsis{S-Lang version of gsl_cdf_tdist_P}
\usage{Double_Type[] cdf_tdist_P (Double_Type[] x, Double_Type[] nu)}
\done

\function{cdf_tdist_Pinv}
\synopsis{S-Lang version of gsl_cdf_tdist_Pinv}
\usage{Double_Type[] cdf_tdist_Pinv (Double_Type[] P, Double_Type[] nu)}
\done

\function{cdf_tdist_Q}
\synopsis{S-Lang version of gsl_cdf_tdist_Q}
\usage{Double_Type[] cdf_tdist_Q (Double_Type[] x, Double_Type[] nu)}
\done

\function{cdf_tdist_Qinv}
\synopsis{S-Lang version of gsl_cdf_tdist_Qinv}
\usage{Double_Type[] cdf_tdist_Qinv (Double_Type[] Q, Double_Type[] nu)}
\done

\function{cdf_ugaussian_P}
\synopsis{S-Lang version of gsl_cdf_ugaussian_P}
\usage{Double_Type[] cdf_ugaussian_P (Double_Type[] x)}
\done

\function{cdf_ugaussian_Pinv}
\synopsis{S-Lang version of gsl_cdf_ugaussian_Pinv}
\usage{Double_Type[] cdf_ugaussian_Pinv (Double_Type[] P)}
\done

\function{cdf_ugaussian_Q}
\synopsis{S-Lang version of gsl_cdf_ugaussian_Q}
\usage{Double_Type[] cdf_ugaussian_Q (Double_Type[] x)}
\done

\function{cdf_ugaussian_Qinv}
\synopsis{S-Lang version of gsl_cdf_ugaussian_Qinv}
\usage{Double_Type[] cdf_ugaussian_Qinv (Double_Type[] Q)}
\done

\function{cdf_weibull_P}
\synopsis{S-Lang version of gsl_cdf_weibull_P}
\usage{Double_Type[] cdf_weibull_P (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_weibull_Pinv}
\synopsis{S-Lang version of gsl_cdf_weibull_Pinv}
\usage{Double_Type[] cdf_weibull_Pinv (P, a, b)}
#v+
  Double_Type[] P
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_weibull_Q}
\synopsis{S-Lang version of gsl_cdf_weibull_Q}
\usage{Double_Type[] cdf_weibull_Q (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{cdf_weibull_Qinv}
\synopsis{S-Lang version of gsl_cdf_weibull_Qinv}
\usage{Double_Type[] cdf_weibull_Qinv (Q, a, b)}
#v+
  Double_Type[] Q
  Double_Type[] a
  Double_Type[] b
#v-
\done

