\function_sect{Random Number Generation Routines}

\function{rng_alloc}
\synopsis{Allocate an instance of a random number generator}
\usage{Rand_Type rng_alloc ([generator])}
\done

\function{rng_set}
\synopsis{Seed a random number generator}
\usage{rng_set ([Rand_Type gen,] ULong_Type seed)}
\done

\function{rng_get}
\synopsis{rng_get}
\usage{x = rng_get ([Rand_Type gen] [, Int_Type num])}
\done

\function{rng_get_rng_types}
\synopsis{Get a list of all supported generators}
\usage{String_Type[] = rng_get_rng_types ()}
\done

\function{rng_uniform}
\synopsis{Get a uniformly distributed random number}
\usage{x = rng_uniform ([Rand_Type gen] [, Int_Type num])}
\done

\function{rng_uniform_pos}
\synopsis{Generate a uniformly distributed non-zero random number}
\usage{x = rng_uniform_pos ([Rand_Type gen] [, Int_Type num])}
\done

   
\function{rng_max}
\synopsis{Obtain the maximum value produced by a random number generator }
\usage{ULong_Type rng_max (Rand_Type gen)}
\done

\function{rng_min}
\synopsis{Obtain the minimum value produced by a random number generator }
\usage{ULong_Type rng_min (Rand_Type gen)}
\done

\function_sect{Random Number Distributions}

\function{ran_bernoulli}
\synopsis{Produce Bernoulli distributed random numbers}
\usage{x = ran_bernoulli ([Rand_Type gen,] Double_Type p [,Int_Type num] }
\done

\function{ran_beta}
\synopsis{Produce distributed random numbers}
\usage{x = ran_beta ([Rand_Type gen,] Double_Type a, Double_Type b [,Int_Type num])}
\done

\function{ran_binomial}
\synopsis{Produce random numbers from the binomial distribution}
\usage{x = ran_binomial ([Rand_Type gen,] Double_Type p, Int_Type n [,Int_Type num])}
\done

\function{ran_cauchy}
\synopsis{Produce random numbers from the Cauchy distribution}
\usage{x = ran_cauchy ([Rand_Type gen,] Double_Type mu [,Int_Type num])}
\done

\function{ran_chisq}
\synopsis{Produce chi-squared distributed random numbers}
\usage{x = ran_chisq ([Rand_Type gen,] Double_Type nu [,Int_Type num])}
\done

#%+
\function{ran_erlang}
\synopsis{Produce distributed random numbers}
\usage{x = ran_erlang ([Rand_Type gen] [,Int_Type num])}
\done
#%-

\function{ran_exponential}
\synopsis{Produce exponentially distributed random numbers}
\usage{x = ran_exponential ([Rand_Type gen,] Double_Type mu [,Int_Type num])}
\done

\function{ran_exppow}
\synopsis{Produce random numbers from the exponential power distribution}
\usage{x = ran_exppow ([Rand_Type gen,] Double_Type mu, Double_Type a [,Int_Type num])}
\done

\function{ran_fdist}
\synopsis{Produce F-distributed random numbers}
\usage{x = ran_fdist ([Rand_Type gen,] Double_Type nu1, Double_Type nu2 [,Int_Type num])}
\done

\function{ran_flat}
\synopsis{Produce uniformly distributed random numbers}
\usage{x = ran_flat ([Rand_Type gen,] Double_Type a, Double_Type b [,Int_Type num])}
\done

\function{ran_gamma}
\synopsis{Produce a random number from the gamma distribution}
\usage{x = ran_gamma ([Rand_Type gen,] Double_Type a, Double_Type b [,Int_Type num])}
\done

#%+
\function{ran_gamma_int}
\synopsis{Produce distributed random numbers}
\usage{x = ran_gamma_int ([Rand_Type gen] [,Int_Type num])}
\done
#%-

\function{ran_gaussian}
\synopsis{Produce gaussian distributed random numbers}
\usage{x = ran_gaussian ([Rand_Type gen,] Double_Type sigma [,Int_Type num])}
\done

\function{ran_gaussian_ratio_method}
\synopsis{Produce gaussian distributed random numbers}
\usage{x = ran_gaussian_ratio_method ([Rand_Type gen,] Double_Type sigma [,Int_Type num])}
\done

\function{ran_gaussian_tail}
\synopsis{Produce gaussian distributed random numbers from the tail}
\usage{x = ran_gaussian_tail ([Rand_Type gen,] Double_Type a, Double_Type sigma [,Int_Type num])}
\done

\function{ran_geometric}
\synopsis{Produce random integers from the geometric distribution}
\usage{x = ran_geometric ([Rand_Type gen,] Double_Type p [,Int_Type num])}
\done

\function{ran_gumbel1}
\synopsis{Produce random numbers from the type-1 Gumbel distribution}
\usage{x = ran_gumbel1 ([Rand_Type gen,] Double_Type a, Double_Type b [,Int_Type num])}
\done

\function{ran_gumbel2}
\synopsis{Produce random numbers from the type-2 Gumbel distribution}
\usage{x = ran_gumbel2 ([Rand_Type gen,] Double_Type a, Double_Type b [,Int_Type num])}
\done

#%+
\function{ran_landau}
\synopsis{Produce random numbers from the landau distribution}
\usage{x = ran_landau ([Rand_Type gen] [,Int_Type num])}
\done
#%-

\function{ran_laplace}
\synopsis{Produce random numbers from the Laplace distribution}
\usage{x = ran_laplace ([Rand_Type gen,] Double_Type mu [,Int_Type num])}
\done

\function{ran_levy}
\synopsis{Produce random numbers from the Levy distribution}
\usage{x = ran_levy ([Rand_Type gen,] Double_Type mu, Double_Type a [,Int_Type num])}
\done

\function{ran_logarithmic}
\synopsis{Produce random numbers from the logarithmic distribution}
\usage{x = ran_logarithmic ([Rand_Type gen,] Double_Type p [,Int_Type num])}
\done

\function{ran_logistic}
\synopsis{Produce random numbers from the logistic distribution}
\usage{x = ran_logistic ([Rand_Type gen,] Double_Type mu [,Int_Type num])}
\done

\function{ran_lognormal}
\synopsis{Produce random numbers from the lognormal distribution}
\usage{x = ran_lognormal ([Rand_Type gen,] Double_Type zeta, Double_Type sigma [,Int_Type num])}
\done

\function{ran_negative_binomial}
\synopsis{Produce random numbers from the negative binomial distribution}
\usage{x = ran_negative_binomial ([Rand_Type gen,] Double_Type p, Double_Type n [,Int_Type num])}
\done

\function{ran_pareto}
\synopsis{Produce random numbers from the Pareto distribution}
\usage{x = ran_pareto ([Rand_Type gen,] Double_Type a, Double_Type b [,Int_Type num])}
\done

\function{ran_pascal}
\synopsis{Produce random numbers from the Pascal distribution}
\usage{x = ran_pascal ([Rand_Type gen,] Double_Type p, Int_Type k [,Int_Type num])}
\done

\function{ran_poisson}
\synopsis{Produce random numbers from the Poisson distribution}
\usage{x = ran_poisson ([Rand_Type gen,] Double_Type mu [,Int_Type num])}
\done

\function{ran_rayleigh}
\synopsis{Produce random numbers from the Rayleigh distribution}
\usage{x = ran_rayleigh ([Rand_Type gen,] Double_Type sigma [,Int_Type num])}
\done

\function{ran_rayleigh_tail}
\synopsis{Produce random numbers from the tail of the Rayleigh distribution}
\usage{x = ran_rayleigh_tail ([Rand_Type gen,] Double_Type a, Double_Type sigma [,Int_Type num])}
\done

\function{ran_tdist}
\synopsis{Produce random numbers from the t-distribution}
\usage{x = ran_tdist ([Rand_Type gen,] Double_Type nu [,Int_Type num])}
\done

\function{ran_ugaussian}
\synopsis{Produce random numbers from the gaussian distribution}
\usage{x = ran_ugaussian ([Rand_Type gen] [,Int_Type num])}
\done

\function{ran_ugaussian_ratio_method}
\synopsis{Produce random numbers from the gaussian distribution}
\usage{x = ran_ugaussian_ratio_method ([Rand_Type gen] [,Int_Type num])}
\done

\function{ran_ugaussian_tail}
\synopsis{Produce random numbers from the tail of the gaussian distribution}
\usage{x = ran_ugaussian_tail ([Rand_Type gen,] Double_Type a [,Int_Type num])}
\done

\function{ran_weibull}
\synopsis{Produce random numbers from the Weibull distribution}
\usage{x = ran_weibull ([Rand_Type gen,] Double_Type mu, Double_Type a [,Int_Type num])}
\done

\function_sect{PDF Functions}
\function{ran_beta_pdf}
\synopsis{S-Lang version of gsl_ran_beta_pdf}
\usage{Double_Type[] ran_beta_pdf (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{ran_cauchy_pdf}
\synopsis{S-Lang version of gsl_ran_cauchy_pdf}
\usage{Double_Type[] ran_cauchy_pdf (Double_Type[] x, Double_Type[] a)}
\done

\function{ran_chisq_pdf}
\synopsis{S-Lang version of gsl_ran_chisq_pdf}
\usage{Double_Type[] ran_chisq_pdf (Double_Type[] x, Double_Type[] nu)}
\done

\function{ran_erlang_pdf}
\synopsis{S-Lang version of gsl_ran_erlang_pdf}
\usage{Double_Type[] ran_erlang_pdf (x, a, n)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] n
#v-
\done

\function{ran_exponential_pdf}
\synopsis{S-Lang version of gsl_ran_exponential_pdf}
\usage{Double_Type[] ran_exponential_pdf (Double_Type[] x, Double_Type[] mu)}
\done

\function{ran_exppow_pdf}
\synopsis{S-Lang version of gsl_ran_exppow_pdf}
\usage{Double_Type[] ran_exppow_pdf (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{ran_fdist_pdf}
\synopsis{S-Lang version of gsl_ran_fdist_pdf}
\usage{Double_Type[] ran_fdist_pdf (x, nu1, nu2)}
#v+
  Double_Type[] x
  Double_Type[] nu1
  Double_Type[] nu2
#v-
\done

\function{ran_flat_pdf}
\synopsis{S-Lang version of gsl_ran_flat_pdf}
\usage{Double_Type[] ran_flat_pdf (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{ran_gamma_pdf}
\synopsis{S-Lang version of gsl_ran_gamma_pdf}
\usage{Double_Type[] ran_gamma_pdf (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{ran_gaussian_pdf}
\synopsis{S-Lang version of gsl_ran_gaussian_pdf}
\usage{Double_Type[] ran_gaussian_pdf (Double_Type[] x, Double_Type[] sigma)}
\done

\function{ran_gaussian_tail_pdf}
\synopsis{S-Lang version of gsl_ran_gaussian_tail_pdf}
\usage{Double_Type[] ran_gaussian_tail_pdf (x, a, sigma)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] sigma
#v-
\done

\function{ran_gumbel1_pdf}
\synopsis{S-Lang version of gsl_ran_gumbel1_pdf}
\usage{Double_Type[] ran_gumbel1_pdf (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{ran_gumbel2_pdf}
\synopsis{S-Lang version of gsl_ran_gumbel2_pdf}
\usage{Double_Type[] ran_gumbel2_pdf (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{ran_landau_pdf}
\synopsis{S-Lang version of gsl_ran_landau_pdf}
\usage{Double_Type[] ran_landau_pdf (Double_Type[] x)}
\done

\function{ran_laplace_pdf}
\synopsis{S-Lang version of gsl_ran_laplace_pdf}
\usage{Double_Type[] ran_laplace_pdf (Double_Type[] x, Double_Type[] a)}
\done

\function{ran_logistic_pdf}
\synopsis{S-Lang version of gsl_ran_logistic_pdf}
\usage{Double_Type[] ran_logistic_pdf (Double_Type[] x, Double_Type[] a)}
\done

\function{ran_lognormal_pdf}
\synopsis{S-Lang version of gsl_ran_lognormal_pdf}
\usage{Double_Type[] ran_lognormal_pdf (x, zeta, sigma)}
#v+
  Double_Type[] x
  Double_Type[] zeta
  Double_Type[] sigma
#v-
\done

\function{ran_pareto_pdf}
\synopsis{S-Lang version of gsl_ran_pareto_pdf}
\usage{Double_Type[] ran_pareto_pdf (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done

\function{ran_rayleigh_pdf}
\synopsis{S-Lang version of gsl_ran_rayleigh_pdf}
\usage{Double_Type[] ran_rayleigh_pdf (Double_Type[] x, Double_Type[] sigma)}
\done

\function{ran_rayleigh_tail_pdf}
\synopsis{S-Lang version of gsl_ran_rayleigh_tail_pdf}
\usage{Double_Type[] ran_rayleigh_tail_pdf (x, a, sigma)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] sigma
#v-
\done

\function{ran_tdist_pdf}
\synopsis{S-Lang version of gsl_ran_tdist_pdf}
\usage{Double_Type[] ran_tdist_pdf (Double_Type[] x, Double_Type[] nu)}
\done

\function{ran_ugaussian_pdf}
\synopsis{S-Lang version of gsl_ran_ugaussian_pdf}
\usage{Double_Type[] ran_ugaussian_pdf (Double_Type[] x)}
\done

\function{ran_ugaussian_tail_pdf}
\synopsis{S-Lang version of gsl_ran_ugaussian_tail_pdf}
\usage{Double_Type[] ran_ugaussian_tail_pdf (Double_Type[] x, Double_Type[] a)}
\done

\function{ran_weibull_pdf}
\synopsis{S-Lang version of gsl_ran_weibull_pdf}
\usage{Double_Type[] ran_weibull_pdf (x, a, b)}
#v+
  Double_Type[] x
  Double_Type[] a
  Double_Type[] b
#v-
\done
