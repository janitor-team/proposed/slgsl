\function_sect{Airy Functions}
\function{airy_Ai}
\synopsis{S-Lang version of gsl_sf_airy_Ai}
\usage{Double_Type[] airy_Ai (Double_Type[] x [,Int_Type mode])}
\done

\function{airy_Ai_deriv}
\synopsis{S-Lang version of gsl_sf_airy_Ai_deriv}
\usage{Double_Type[] airy_Ai_deriv (Double_Type[] x [,Int_Type mode])}
\done

\function{airy_Ai_deriv_scaled}
\synopsis{S-Lang version of gsl_sf_airy_Ai_deriv_scaled}
\usage{Double_Type[] airy_Ai_deriv_scaled (Double_Type[] x [,Int_Type mode])}
\done

\function{airy_Ai_scaled}
\synopsis{S-Lang version of gsl_sf_airy_Ai_scaled}
\usage{Double_Type[] airy_Ai_scaled (Double_Type[] x [,Int_Type mode])}
\done

\function{airy_Bi}
\synopsis{S-Lang version of gsl_sf_airy_Bi}
\usage{Double_Type[] airy_Bi (Double_Type[] x [,Int_Type mode])}
\done

\function{airy_Bi_deriv}
\synopsis{S-Lang version of gsl_sf_airy_Bi_deriv}
\usage{Double_Type[] airy_Bi_deriv (Double_Type[] x [,Int_Type mode])}
\done

\function{airy_Bi_deriv_scaled}
\synopsis{S-Lang version of gsl_sf_airy_Bi_deriv_scaled}
\usage{Double_Type[] airy_Bi_deriv_scaled (Double_Type[] x [,Int_Type mode])}
\done

\function{airy_Bi_scaled}
\synopsis{S-Lang version of gsl_sf_airy_Bi_scaled}
\usage{Double_Type[] airy_Bi_scaled (Double_Type[] x [,Int_Type mode])}
\done

\function_sect{Bessel Functions}
\function{bessel_I0}
\synopsis{S-Lang version of gsl_sf_bessel_I0}
\usage{Double_Type[] bessel_I0 (Double_Type[] x)}
\done

\function{bessel_i0_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_i0_scaled}
\usage{Double_Type[] bessel_i0_scaled (Double_Type[] x)}
\done

\function{bessel_I0_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_I0_scaled}
\usage{Double_Type[] bessel_I0_scaled (Double_Type[] x)}
\done

\function{bessel_I1}
\synopsis{S-Lang version of gsl_sf_bessel_I1}
\usage{Double_Type[] bessel_I1 (Double_Type[] x)}
\done

\function{bessel_i1_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_i1_scaled}
\usage{Double_Type[] bessel_i1_scaled (Double_Type[] x)}
\done

\function{bessel_I1_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_I1_scaled}
\usage{Double_Type[] bessel_I1_scaled (Double_Type[] x)}
\done

\function{bessel_i2_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_i2_scaled}
\usage{Double_Type[] bessel_i2_scaled (Double_Type[] x)}
\done

\function{bessel_il_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_il_scaled}
\usage{Double_Type[] bessel_il_scaled (Int_Type[] l, Double_Type[] x)}
\done

\function{bessel_In}
\synopsis{S-Lang version of gsl_sf_bessel_In}
\usage{Double_Type[] bessel_In (Int_Type[] n, Double_Type[] x)}
\done

\function{bessel_In_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_In_scaled}
\usage{Double_Type[] bessel_In_scaled (Int_Type[] n, Double_Type[] x)}
\done

\function{bessel_Inu}
\synopsis{S-Lang version of gsl_sf_bessel_Inu}
\usage{Double_Type[] bessel_Inu (Double_Type[] nu, Double_Type[] x)}
\done

\function{bessel_Inu_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_Inu_scaled}
\usage{Double_Type[] bessel_Inu_scaled (Double_Type[] nu, Double_Type[] x)}
\done

\function{bessel_J0}
\synopsis{S-Lang version of gsl_sf_bessel_J0}
\usage{Double_Type[] bessel_J0 (Double_Type[] x)}
\done

\function{bessel_j0}
\synopsis{S-Lang version of gsl_sf_bessel_j0}
\usage{Double_Type[] bessel_j0 (Double_Type[] x)}
\done

\function{bessel_j1}
\synopsis{S-Lang version of gsl_sf_bessel_j1}
\usage{Double_Type[] bessel_j1 (Double_Type[] x)}
\done

\function{bessel_J1}
\synopsis{S-Lang version of gsl_sf_bessel_J1}
\usage{Double_Type[] bessel_J1 (Double_Type[] x)}
\done

\function{bessel_j2}
\synopsis{S-Lang version of gsl_sf_bessel_j2}
\usage{Double_Type[] bessel_j2 (Double_Type[] x)}
\done

\function{bessel_jl}
\synopsis{S-Lang version of gsl_sf_bessel_jl}
\usage{Double_Type[] bessel_jl (Int_Type[] l, Double_Type[] x)}
\done

\function{bessel_Jn}
\synopsis{S-Lang version of gsl_sf_bessel_Jn}
\usage{Double_Type[] bessel_Jn (Int_Type[] n, Double_Type[] x)}
\done

\function{bessel_Jnu}
\synopsis{S-Lang version of gsl_sf_bessel_Jnu}
\usage{Double_Type[] bessel_Jnu (Double_Type[] nu, Double_Type[] x)}
\done

\function{bessel_K0}
\synopsis{S-Lang version of gsl_sf_bessel_K0}
\usage{Double_Type[] bessel_K0 (Double_Type[] x)}
\done

\function{bessel_K0_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_K0_scaled}
\usage{Double_Type[] bessel_K0_scaled (Double_Type[] x)}
\done

\function{bessel_k0_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_k0_scaled}
\usage{Double_Type[] bessel_k0_scaled (Double_Type[] x)}
\done

\function{bessel_K1}
\synopsis{S-Lang version of gsl_sf_bessel_K1}
\usage{Double_Type[] bessel_K1 (Double_Type[] x)}
\done

\function{bessel_K1_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_K1_scaled}
\usage{Double_Type[] bessel_K1_scaled (Double_Type[] x)}
\done

\function{bessel_k1_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_k1_scaled}
\usage{Double_Type[] bessel_k1_scaled (Double_Type[] x)}
\done

\function{bessel_k2_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_k2_scaled}
\usage{Double_Type[] bessel_k2_scaled (Double_Type[] x)}
\done

\function{bessel_kl_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_kl_scaled}
\usage{Double_Type[] bessel_kl_scaled (Int_Type[] l, Double_Type[] x)}
\done

\function{bessel_Kn}
\synopsis{S-Lang version of gsl_sf_bessel_Kn}
\usage{Double_Type[] bessel_Kn (Int_Type[] n, Double_Type[] x)}
\done

\function{bessel_Kn_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_Kn_scaled}
\usage{Double_Type[] bessel_Kn_scaled (Int_Type[] n, Double_Type[] x)}
\done

\function{bessel_Knu}
\synopsis{S-Lang version of gsl_sf_bessel_Knu}
\usage{Double_Type[] bessel_Knu (Double_Type[] nu, Double_Type[] x)}
\done

\function{bessel_Knu_scaled}
\synopsis{S-Lang version of gsl_sf_bessel_Knu_scaled}
\usage{Double_Type[] bessel_Knu_scaled (Double_Type[] nu, Double_Type[] x)}
\done

\function{bessel_lnKnu}
\synopsis{S-Lang version of gsl_sf_bessel_lnKnu}
\usage{Double_Type[] bessel_lnKnu (Double_Type[] nu, Double_Type[] x)}
\done

\function{bessel_y0}
\synopsis{S-Lang version of gsl_sf_bessel_y0}
\usage{Double_Type[] bessel_y0 (Double_Type[] x)}
\done

\function{bessel_Y0}
\synopsis{S-Lang version of gsl_sf_bessel_Y0}
\usage{Double_Type[] bessel_Y0 (Double_Type[] x)}
\done

\function{bessel_y1}
\synopsis{S-Lang version of gsl_sf_bessel_y1}
\usage{Double_Type[] bessel_y1 (Double_Type[] x)}
\done

\function{bessel_Y1}
\synopsis{S-Lang version of gsl_sf_bessel_Y1}
\usage{Double_Type[] bessel_Y1 (Double_Type[] x)}
\done

\function{bessel_y2}
\synopsis{S-Lang version of gsl_sf_bessel_y2}
\usage{Double_Type[] bessel_y2 (Double_Type[] x)}
\done

\function{bessel_yl}
\synopsis{S-Lang version of gsl_sf_bessel_yl}
\usage{Double_Type[] bessel_yl (Int_Type[] l, Double_Type[] x)}
\done

\function{bessel_Yn}
\synopsis{S-Lang version of gsl_sf_bessel_Yn}
\usage{Double_Type[] bessel_Yn (Int_Type[] n, Double_Type[] x)}
\done

\function{bessel_Ynu}
\synopsis{S-Lang version of gsl_sf_bessel_Ynu}
\usage{Double_Type[] bessel_Ynu (Double_Type[] nu, Double_Type[] x)}
\done

\function_sect{Beta Functions}
\function{beta}
\synopsis{S-Lang version of gsl_sf_beta}
\usage{Double_Type[] beta (Double_Type[] a, Double_Type[] b)}
\done

\function{beta_inc}
\synopsis{S-Lang version of gsl_sf_beta_inc}
\usage{Double_Type[] beta_inc (Double_Type[] a, Double_Type[] b, Double_Type[] x)}
\done

\function{lnbeta}
\synopsis{S-Lang version of gsl_sf_lnbeta}
\usage{Double_Type[] lnbeta (Double_Type[] a, Double_Type[] b)}
\done

\function_sect{Clausen Functions}
\function{clausen}
\synopsis{S-Lang version of gsl_sf_clausen}
\usage{Double_Type[] clausen (Double_Type[] x)}
\done

\function_sect{Conical Functions}
\function{conicalP_0}
\synopsis{S-Lang version of gsl_sf_conicalP_0}
\usage{Double_Type[] conicalP_0 (Double_Type[] lambda, Double_Type[] x)}
\done

\function{conicalP_1}
\synopsis{S-Lang version of gsl_sf_conicalP_1}
\usage{Double_Type[] conicalP_1 (Double_Type[] lambda, Double_Type[] x)}
\done

\function{conicalP_cyl_reg}
\synopsis{S-Lang version of gsl_sf_conicalP_cyl_reg}
\usage{Double_Type[] conicalP_cyl_reg (m, lambda, x)}
#v+
  Int_Type[] m
  Double_Type[] lambda
  Double_Type[] x
#v-
\done

\function{conicalP_half}
\synopsis{S-Lang version of gsl_sf_conicalP_half}
\usage{Double_Type[] conicalP_half (Double_Type[] lambda, Double_Type[] x)}
\done

\function{conicalP_mhalf}
\synopsis{S-Lang version of gsl_sf_conicalP_mhalf}
\usage{Double_Type[] conicalP_mhalf (Double_Type[] lambda, Double_Type[] x)}
\done

\function{conicalP_sph_reg}
\synopsis{S-Lang version of gsl_sf_conicalP_sph_reg}
\usage{Double_Type[] conicalP_sph_reg (l, lambda, x)}
#v+
  Int_Type[] l
  Double_Type[] lambda
  Double_Type[] x
#v-
\done

\function_sect{Coulomb Functions}
\function{hydrogenicR}
\synopsis{S-Lang version of gsl_sf_hydrogenicR}
\usage{Double_Type[] hydrogenicR (n, l, Z, r)}
#v+
  Int_Type[] n
  Int_Type[] l
  Double_Type[] Z
  Double_Type[] r
#v-
\done

\function{hydrogenicR_1}
\synopsis{S-Lang version of gsl_sf_hydrogenicR_1}
\usage{Double_Type[] hydrogenicR_1 (Double_Type[] Z, Double_Type[] r)}
\done

\function_sect{Debye Functions}
\function{debye_1}
\synopsis{S-Lang version of gsl_sf_debye_1}
\usage{Double_Type[] debye_1 (Double_Type[] x)}
\done

\function{debye_2}
\synopsis{S-Lang version of gsl_sf_debye_2}
\usage{Double_Type[] debye_2 (Double_Type[] x)}
\done

\function{debye_3}
\synopsis{S-Lang version of gsl_sf_debye_3}
\usage{Double_Type[] debye_3 (Double_Type[] x)}
\done

\function{debye_4}
\synopsis{S-Lang version of gsl_sf_debye_4}
\usage{Double_Type[] debye_4 (Double_Type[] x)}
\done

\function{debye_5}
\synopsis{S-Lang version of gsl_sf_debye_5}
\usage{Double_Type[] debye_5 (Double_Type[] x)}
\done

\function{debye_6}
\synopsis{S-Lang version of gsl_sf_debye_6}
\usage{Double_Type[] debye_6 (Double_Type[] x)}
\done

\function_sect{Di/Tri and Polygamma Functions}
\function{psi}
\synopsis{S-Lang version of gsl_sf_psi}
\usage{Double_Type[] psi (Double_Type[] x)}
\done

\function{psi_1}
\synopsis{S-Lang version of gsl_sf_psi_1}
\usage{Double_Type[] psi_1 (Double_Type[] x)}
\done

\function{psi_1_int}
\synopsis{S-Lang version of gsl_sf_psi_1_int}
\usage{Double_Type[] psi_1_int (Int_Type[] n)}
\done

\function{psi_1piy}
\synopsis{S-Lang version of gsl_sf_psi_1piy}
\usage{Double_Type[] psi_1piy (Double_Type[] y)}
\done

\function{psi_int}
\synopsis{S-Lang version of gsl_sf_psi_int}
\usage{Double_Type[] psi_int (Int_Type[] n)}
\done

\function{psi_n}
\synopsis{S-Lang version of gsl_sf_psi_n}
\usage{Double_Type[] psi_n (Int_Type[] n, Double_Type[] x)}
\done

\function_sect{Elliptic Integrals}
\function{ellint_D}
\synopsis{S-Lang version of gsl_sf_ellint_D}
\usage{Double_Type[] ellint_D (phi, k [,mode])}
#v+
  Double_Type[] phi
  Double_Type[] k 
  Int_Type mode
#v-
\done

\function{ellint_Dcomp}
\synopsis{S-Lang version of gsl_sf_ellint_Dcomp}
\usage{Double_Type[] ellint_Dcomp (Double_Type[] k [,Int_Type mode])}
\done

\function{ellint_E}
\synopsis{S-Lang version of gsl_sf_ellint_E}
\usage{Double_Type[] ellint_E (phi, k [,mode])}
#v+
  Double_Type[] phi
  Double_Type[] k 
  Int_Type mode
#v-
\done

\function{ellint_Ecomp}
\synopsis{S-Lang version of gsl_sf_ellint_Ecomp}
\usage{Double_Type[] ellint_Ecomp (Double_Type[] k [,Int_Type mode])}
\done

\function{ellint_F}
\synopsis{S-Lang version of gsl_sf_ellint_F}
\usage{Double_Type[] ellint_F (phi, k [,mode])}
#v+
  Double_Type[] phi
  Double_Type[] k 
  Int_Type mode
#v-
\done

\function{ellint_Kcomp}
\synopsis{S-Lang version of gsl_sf_ellint_Kcomp}
\usage{Double_Type[] ellint_Kcomp (Double_Type[] k [,Int_Type mode])}
\done

\function{ellint_P}
\synopsis{S-Lang version of gsl_sf_ellint_P}
\usage{Double_Type[] ellint_P (phi, k, n [,mode])}
#v+
  Double_Type[] phi
  Double_Type[] k
  Double_Type[] n 
  Int_Type mode
#v-
\done

\function{ellint_Pcomp}
\synopsis{S-Lang version of gsl_sf_ellint_Pcomp}
\usage{Double_Type[] ellint_Pcomp (k, n [,mode])}
#v+
  Double_Type[] k
  Double_Type[] n 
  Int_Type mode
#v-
\done

\function{ellint_RC}
\synopsis{S-Lang version of gsl_sf_ellint_RC}
\usage{Double_Type[] ellint_RC (Double_Type[] x, Double_Type[] y [,Int_Type mode])}
\done

\function{ellint_RD}
\synopsis{S-Lang version of gsl_sf_ellint_RD}
\usage{Double_Type[] ellint_RD (x, y, z [,mode])}
#v+
  Double_Type[] x
  Double_Type[] y
  Double_Type[] z 
  Int_Type mode
#v-
\done

\function{ellint_RF}
\synopsis{S-Lang version of gsl_sf_ellint_RF}
\usage{Double_Type[] ellint_RF (x, y, z [,mode])}
#v+
  Double_Type[] x
  Double_Type[] y
  Double_Type[] z 
  Int_Type mode
#v-
\done

\function{ellint_RJ}
\synopsis{S-Lang version of gsl_sf_ellint_RJ}
\usage{Double_Type[] ellint_RJ (x, y, z, p [,mode])}
#v+
  Double_Type[] x
  Double_Type[] y
  Double_Type[] z
  Double_Type[] p 
  Int_Type mode
#v-
\done

\function_sect{Error Functions}
\function{erf}
\synopsis{S-Lang version of gsl_sf_erf}
\usage{Double_Type[] erf (Double_Type[] x)}
\done

\function{erf_Q}
\synopsis{S-Lang version of gsl_sf_erf_Q}
\usage{Double_Type[] erf_Q (Double_Type[] x)}
\done

\function{erf_Z}
\synopsis{S-Lang version of gsl_sf_erf_Z}
\usage{Double_Type[] erf_Z (Double_Type[] x)}
\done

\function{erfc}
\synopsis{S-Lang version of gsl_sf_erfc}
\usage{Double_Type[] erfc (Double_Type[] x)}
\done

\function{log_erfc}
\synopsis{S-Lang version of gsl_sf_log_erfc}
\usage{Double_Type[] log_erfc (Double_Type[] x)}
\done

\function_sect{Eta/Zeta Functions}
\function{eta}
\synopsis{S-Lang version of gsl_sf_eta}
\usage{Double_Type[] eta (Double_Type[] s)}
\done

\function{eta_int}
\synopsis{S-Lang version of gsl_sf_eta_int}
\usage{Double_Type[] eta_int (Int_Type[] n)}
\done

\function{hzeta}
\synopsis{S-Lang version of gsl_sf_hzeta}
\usage{Double_Type[] hzeta (Double_Type[] s, Double_Type[] q)}
\done

\function{zeta}
\synopsis{S-Lang version of gsl_sf_zeta}
\usage{Double_Type[] zeta (Double_Type[] s)}
\done

\function{zeta_int}
\synopsis{S-Lang version of gsl_sf_zeta_int}
\usage{Double_Type[] zeta_int (Int_Type[] n)}
\done

\function{zetam1}
\synopsis{S-Lang version of gsl_sf_zetam1}
\usage{Double_Type[] zetam1 (Double_Type[] s)}
\done

\function{zetam1_int}
\synopsis{S-Lang version of gsl_sf_zetam1_int}
\usage{Double_Type[] zetam1_int (Int_Type[] s)}
\done

\function_sect{Exponential Functions and Integrals}
\function{exp_mult}
\synopsis{S-Lang version of gsl_sf_exp_mult}
\usage{Double_Type[] exp_mult (Double_Type[] x, Double_Type[] y)}
\done

\function{expint_3}
\synopsis{S-Lang version of gsl_sf_expint_3}
\usage{Double_Type[] expint_3 (Double_Type[] x)}
\done

\function{expint_E1}
\synopsis{S-Lang version of gsl_sf_expint_E1}
\usage{Double_Type[] expint_E1 (Double_Type[] x)}
\done

\function{expint_E1_scaled}
\synopsis{S-Lang version of gsl_sf_expint_E1_scaled}
\usage{Double_Type[] expint_E1_scaled (Double_Type[] x)}
\done

\function{expint_E2}
\synopsis{S-Lang version of gsl_sf_expint_E2}
\usage{Double_Type[] expint_E2 (Double_Type[] x)}
\done

\function{expint_E2_scaled}
\synopsis{S-Lang version of gsl_sf_expint_E2_scaled}
\usage{Double_Type[] expint_E2_scaled (Double_Type[] x)}
\done

\function{expint_Ei}
\synopsis{S-Lang version of gsl_sf_expint_Ei}
\usage{Double_Type[] expint_Ei (Double_Type[] x)}
\done

\function{expint_Ei_scaled}
\synopsis{S-Lang version of gsl_sf_expint_Ei_scaled}
\usage{Double_Type[] expint_Ei_scaled (Double_Type[] x)}
\done

\function{expint_En}
\synopsis{S-Lang version of gsl_sf_expint_En}
\usage{Double_Type[] expint_En (Int_Type[] n, Double_Type[] x)}
\done

\function{expint_En_scaled}
\synopsis{S-Lang version of gsl_sf_expint_En_scaled}
\usage{Double_Type[] expint_En_scaled (Int_Type[] n, Double_Type[] x)}
\done

\function{expm1}
\synopsis{S-Lang version of gsl_sf_expm1}
\usage{Double_Type[] expm1 (Double_Type[] x)}
\done

\function{exprel}
\synopsis{S-Lang version of gsl_sf_exprel}
\usage{Double_Type[] exprel (Double_Type[] x)}
\done

\function{exprel_2}
\synopsis{S-Lang version of gsl_sf_exprel_2}
\usage{Double_Type[] exprel_2 (Double_Type[] x)}
\done

\function{exprel_n}
\synopsis{S-Lang version of gsl_sf_exprel_n}
\usage{Double_Type[] exprel_n (Int_Type[] n, Double_Type[] x)}
\done

\function_sect{Fermi-Dirac Functions}
\function{fermi_dirac_0}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_0}
\usage{Double_Type[] fermi_dirac_0 (Double_Type[] x)}
\done

\function{fermi_dirac_1}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_1}
\usage{Double_Type[] fermi_dirac_1 (Double_Type[] x)}
\done

\function{fermi_dirac_2}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_2}
\usage{Double_Type[] fermi_dirac_2 (Double_Type[] x)}
\done

\function{fermi_dirac_3half}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_3half}
\usage{Double_Type[] fermi_dirac_3half (Double_Type[] x)}
\done

\function{fermi_dirac_half}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_half}
\usage{Double_Type[] fermi_dirac_half (Double_Type[] x)}
\done

\function{fermi_dirac_inc_0}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_inc_0}
\usage{Double_Type[] fermi_dirac_inc_0 (Double_Type[] x, Double_Type[] b)}
\done

\function{fermi_dirac_int}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_int}
\usage{Double_Type[] fermi_dirac_int (Int_Type[] j, Double_Type[] x)}
\done

\function{fermi_dirac_m1}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_m1}
\usage{Double_Type[] fermi_dirac_m1 (Double_Type[] x)}
\done

\function{fermi_dirac_mhalf}
\synopsis{S-Lang version of gsl_sf_fermi_dirac_mhalf}
\usage{Double_Type[] fermi_dirac_mhalf (Double_Type[] x)}
\done

\function_sect{Gamma Functions}
\function{gamma}
\synopsis{S-Lang version of gsl_sf_gamma}
\usage{Double_Type[] gamma (Double_Type[] x)}
\done

\function{gamma_inc}
\synopsis{S-Lang version of gsl_sf_gamma_inc}
\usage{Double_Type[] gamma_inc (Double_Type[] a, Double_Type[] x)}
\done

\function{gamma_inc_P}
\synopsis{S-Lang version of gsl_sf_gamma_inc_P}
\usage{Double_Type[] gamma_inc_P (Double_Type[] a, Double_Type[] x)}
\done

\function{gamma_inc_Q}
\synopsis{S-Lang version of gsl_sf_gamma_inc_Q}
\usage{Double_Type[] gamma_inc_Q (Double_Type[] a, Double_Type[] x)}
\done

\function{gammainv}
\synopsis{S-Lang version of gsl_sf_gammainv}
\usage{Double_Type[] gammainv (Double_Type[] x)}
\done

\function{gammastar}
\synopsis{S-Lang version of gsl_sf_gammastar}
\usage{Double_Type[] gammastar (Double_Type[] x)}
\done

\function{lngamma}
\synopsis{S-Lang version of gsl_sf_lngamma}
\usage{Double_Type[] lngamma (Double_Type[] x)}
\done

\function_sect{Gegenbauer Functions}
\function{gegenpoly_1}
\synopsis{S-Lang version of gsl_sf_gegenpoly_1}
\usage{Double_Type[] gegenpoly_1 (Double_Type[] lambda, Double_Type[] x)}
\done

\function{gegenpoly_2}
\synopsis{S-Lang version of gsl_sf_gegenpoly_2}
\usage{Double_Type[] gegenpoly_2 (Double_Type[] lambda, Double_Type[] x)}
\done

\function{gegenpoly_3}
\synopsis{S-Lang version of gsl_sf_gegenpoly_3}
\usage{Double_Type[] gegenpoly_3 (Double_Type[] lambda, Double_Type[] x)}
\done

\function{gegenpoly_n}
\synopsis{S-Lang version of gsl_sf_gegenpoly_n}
\usage{Double_Type[] gegenpoly_n (n, lambda, x)}
#v+
  Int_Type[] n
  Double_Type[] lambda
  Double_Type[] x
#v-
\done

\function_sect{Hypergeometric Functions}
\function{hyperg_0F1}
\synopsis{S-Lang version of gsl_sf_hyperg_0F1}
\usage{Double_Type[] hyperg_0F1 (Double_Type[] c, Double_Type[] x)}
\done

\function{hyperg_1F1}
\synopsis{S-Lang version of gsl_sf_hyperg_1F1}
\usage{Double_Type[] hyperg_1F1 (a, b, x)}
#v+
  Double_Type[] a
  Double_Type[] b
  Double_Type[] x
#v-
\done

\function{hyperg_1F1_int}
\synopsis{S-Lang version of gsl_sf_hyperg_1F1_int}
\usage{Double_Type[] hyperg_1F1_int (Int_Type[] m, Int_Type[] n, Double_Type[] x)}
\done

\function{hyperg_2F0}
\synopsis{S-Lang version of gsl_sf_hyperg_2F0}
\usage{Double_Type[] hyperg_2F0 (a, b, x)}
#v+
  Double_Type[] a
  Double_Type[] b
  Double_Type[] x
#v-
\done

\function{hyperg_2F1}
\synopsis{S-Lang version of gsl_sf_hyperg_2F1}
\usage{Double_Type[] hyperg_2F1 (a, b, c, x)}
#v+
  Double_Type[] a
  Double_Type[] b
  Double_Type[] c
  Double_Type[] x
#v-
\done

\function{hyperg_2F1_conj}
\synopsis{S-Lang version of gsl_sf_hyperg_2F1_conj}
\usage{Double_Type[] hyperg_2F1_conj (aR, aI, c, x)}
#v+
  Double_Type[] aR
  Double_Type[] aI
  Double_Type[] c
  Double_Type[] x
#v-
\done

\function{hyperg_2F1_conj_renorm}
\synopsis{S-Lang version of gsl_sf_hyperg_2F1_conj_renorm}
\usage{Double_Type[] hyperg_2F1_conj_renorm (aR, aI, c, x)}
#v+
  Double_Type[] aR
  Double_Type[] aI
  Double_Type[] c
  Double_Type[] x
#v-
\done

\function{hyperg_2F1_renorm}
\synopsis{S-Lang version of gsl_sf_hyperg_2F1_renorm}
\usage{Double_Type[] hyperg_2F1_renorm (a, b, c, x)}
#v+
  Double_Type[] a
  Double_Type[] b
  Double_Type[] c
  Double_Type[] x
#v-
\done

\function{hyperg_U}
\synopsis{S-Lang version of gsl_sf_hyperg_U}
\usage{Double_Type[] hyperg_U (Double_Type[] a, Double_Type[] b, Double_Type[] x)}
\done

\function{hyperg_U_int}
\synopsis{S-Lang version of gsl_sf_hyperg_U_int}
\usage{Double_Type[] hyperg_U_int (Int_Type[] m, Int_Type[] n, Double_Type[] x)}
\done

\function_sect{Laguerre Functions}
\function{laguerre_1}
\synopsis{S-Lang version of gsl_sf_laguerre_1}
\usage{Double_Type[] laguerre_1 (Double_Type[] a, Double_Type[] x)}
\done

\function{laguerre_2}
\synopsis{S-Lang version of gsl_sf_laguerre_2}
\usage{Double_Type[] laguerre_2 (Double_Type[] a, Double_Type[] x)}
\done

\function{laguerre_3}
\synopsis{S-Lang version of gsl_sf_laguerre_3}
\usage{Double_Type[] laguerre_3 (Double_Type[] a, Double_Type[] x)}
\done

\function{laguerre_n}
\synopsis{S-Lang version of gsl_sf_laguerre_n}
\usage{Double_Type[] laguerre_n (Int_Type[] n, Double_Type[] a, Double_Type[] x)}
\done

\function_sect{Lambert Functions}
\function{lambert_W0}
\synopsis{S-Lang version of gsl_sf_lambert_W0}
\usage{Double_Type[] lambert_W0 (Double_Type[] x)}
\done

\function{lambert_Wm1}
\synopsis{S-Lang version of gsl_sf_lambert_Wm1}
\usage{Double_Type[] lambert_Wm1 (Double_Type[] x)}
\done

\function_sect{Legendre Functions and Spherical Harmonics}
\function{legendre_H3d}
\synopsis{S-Lang version of gsl_sf_legendre_H3d}
\usage{Double_Type[] legendre_H3d (l, lambda, eta)}
#v+
  Int_Type[] l
  Double_Type[] lambda
  Double_Type[] eta
#v-
\done

\function{legendre_H3d_0}
\synopsis{S-Lang version of gsl_sf_legendre_H3d_0}
\usage{Double_Type[] legendre_H3d_0 (Double_Type[] lambda, Double_Type[] eta)}
\done

\function{legendre_H3d_1}
\synopsis{S-Lang version of gsl_sf_legendre_H3d_1}
\usage{Double_Type[] legendre_H3d_1 (Double_Type[] lambda, Double_Type[] eta)}
\done

\function{legendre_P1}
\synopsis{S-Lang version of gsl_sf_legendre_P1}
\usage{Double_Type[] legendre_P1 (Double_Type[] x)}
\done

\function{legendre_P2}
\synopsis{S-Lang version of gsl_sf_legendre_P2}
\usage{Double_Type[] legendre_P2 (Double_Type[] x)}
\done

\function{legendre_P3}
\synopsis{S-Lang version of gsl_sf_legendre_P3}
\usage{Double_Type[] legendre_P3 (Double_Type[] x)}
\done

\function{legendre_Pl}
\synopsis{S-Lang version of gsl_sf_legendre_Pl}
\usage{Double_Type[] legendre_Pl (Int_Type[] l, Double_Type[] x)}
\done

\function{legendre_Plm}
\synopsis{S-Lang version of gsl_sf_legendre_Plm}
\usage{Double_Type[] legendre_Plm (Int_Type[] l, Int_Type[] m, Double_Type[] x)}
\done

\function{legendre_Q0}
\synopsis{S-Lang version of gsl_sf_legendre_Q0}
\usage{Double_Type[] legendre_Q0 (Double_Type[] x)}
\done

\function{legendre_Q1}
\synopsis{S-Lang version of gsl_sf_legendre_Q1}
\usage{Double_Type[] legendre_Q1 (Double_Type[] x)}
\done

\function{legendre_Ql}
\synopsis{S-Lang version of gsl_sf_legendre_Ql}
\usage{Double_Type[] legendre_Ql (Int_Type[] l, Double_Type[] x)}
\done

\function{legendre_sphPlm}
\synopsis{S-Lang version of gsl_sf_legendre_sphPlm}
\usage{Double_Type[] legendre_sphPlm (Int_Type[] l, Int_Type[] m, Double_Type[] x)}
\done

\function_sect{Logarithm and Related Functions}
\function{log_1plusx}
\synopsis{S-Lang version of gsl_sf_log_1plusx}
\usage{Double_Type[] log_1plusx (Double_Type[] x)}
\done

\function{log_1plusx_mx}
\synopsis{S-Lang version of gsl_sf_log_1plusx_mx}
\usage{Double_Type[] log_1plusx_mx (Double_Type[] x)}
\done

\function{log_abs}
\synopsis{S-Lang version of gsl_sf_log_abs}
\usage{Double_Type[] log_abs (Double_Type[] x)}
\done

\function_sect{Transport Functions}
\function{transport_2}
\synopsis{S-Lang version of gsl_sf_transport_2}
\usage{Double_Type[] transport_2 (Double_Type[] x)}
\done

\function{transport_3}
\synopsis{S-Lang version of gsl_sf_transport_3}
\usage{Double_Type[] transport_3 (Double_Type[] x)}
\done

\function{transport_4}
\synopsis{S-Lang version of gsl_sf_transport_4}
\usage{Double_Type[] transport_4 (Double_Type[] x)}
\done

\function{transport_5}
\synopsis{S-Lang version of gsl_sf_transport_5}
\usage{Double_Type[] transport_5 (Double_Type[] x)}
\done

\function_sect{Miscellaneous Functions}
\function{angle_restrict_pos}
\synopsis{S-Lang version of gsl_sf_angle_restrict_pos}
\usage{Double_Type[] angle_restrict_pos (Double_Type[] theta)}
\done

\function{angle_restrict_symm}
\synopsis{S-Lang version of gsl_sf_angle_restrict_symm}
\usage{Double_Type[] angle_restrict_symm (Double_Type[] theta)}
\done

\function{atanint}
\synopsis{S-Lang version of gsl_sf_atanint}
\usage{Double_Type[] atanint (Double_Type[] x)}
\done

\function{Chi}
\synopsis{S-Lang version of gsl_sf_Chi}
\usage{Double_Type[] Chi (Double_Type[] x)}
\done

\function{Ci}
\synopsis{S-Lang version of gsl_sf_Ci}
\usage{Double_Type[] Ci (Double_Type[] x)}
\done

\function{dawson}
\synopsis{S-Lang version of gsl_sf_dawson}
\usage{Double_Type[] dawson (Double_Type[] x)}
\done

\function{dilog}
\synopsis{S-Lang version of gsl_sf_dilog}
\usage{Double_Type[] dilog (Double_Type[] x)}
\done

\function{hazard}
\synopsis{S-Lang version of gsl_sf_hazard}
\usage{Double_Type[] hazard (Double_Type[] x)}
\done

\function{hypot}
\synopsis{S-Lang version of gsl_sf_hypot}
\usage{Double_Type[] hypot (Double_Type[] x, Double_Type[] y)}
\done

\function{lncosh}
\synopsis{S-Lang version of gsl_sf_lncosh}
\usage{Double_Type[] lncosh (Double_Type[] x)}
\done

\function{lnpoch}
\synopsis{S-Lang version of gsl_sf_lnpoch}
\usage{Double_Type[] lnpoch (Double_Type[] a, Double_Type[] x)}
\done

\function{lnsinh}
\synopsis{S-Lang version of gsl_sf_lnsinh}
\usage{Double_Type[] lnsinh (Double_Type[] x)}
\done

\function{mathieu_a}
\synopsis{S-Lang version of gsl_sf_mathieu_a}
\usage{Double_Type[] mathieu_a (Int_Type[] order, Double_Type[] qq)}
\done

\function{mathieu_b}
\synopsis{S-Lang version of gsl_sf_mathieu_b}
\usage{Double_Type[] mathieu_b (Int_Type[] order, Double_Type[] qq)}
\done

\function{mathieu_ce}
\synopsis{S-Lang version of gsl_sf_mathieu_ce}
\usage{Double_Type[] mathieu_ce (order, qq, zz)}
#v+
  Int_Type[] order
  Double_Type[] qq
  Double_Type[] zz
#v-
\done

\function{mathieu_Mc}
\synopsis{S-Lang version of gsl_sf_mathieu_Mc}
\usage{Double_Type[] mathieu_Mc (kind, order, qq, zz)}
#v+
  Int_Type[] kind
  Int_Type[] order
  Double_Type[] qq
  Double_Type[] zz
#v-
\done

\function{mathieu_Ms}
\synopsis{S-Lang version of gsl_sf_mathieu_Ms}
\usage{Double_Type[] mathieu_Ms (kind, order, qq, zz)}
#v+
  Int_Type[] kind
  Int_Type[] order
  Double_Type[] qq
  Double_Type[] zz
#v-
\done

\function{mathieu_se}
\synopsis{S-Lang version of gsl_sf_mathieu_se}
\usage{Double_Type[] mathieu_se (order, qq, zz)}
#v+
  Int_Type[] order
  Double_Type[] qq
  Double_Type[] zz
#v-
\done

\function{poch}
\synopsis{S-Lang version of gsl_sf_poch}
\usage{Double_Type[] poch (Double_Type[] a, Double_Type[] x)}
\done

\function{pochrel}
\synopsis{S-Lang version of gsl_sf_pochrel}
\usage{Double_Type[] pochrel (Double_Type[] a, Double_Type[] x)}
\done

\function{Shi}
\synopsis{S-Lang version of gsl_sf_Shi}
\usage{Double_Type[] Shi (Double_Type[] x)}
\done

\function{Si}
\synopsis{S-Lang version of gsl_sf_Si}
\usage{Double_Type[] Si (Double_Type[] x)}
\done

\function{sinc}
\synopsis{S-Lang version of gsl_sf_sinc}
\usage{Double_Type[] sinc (Double_Type[] x)}
\done

\function{synchrotron_1}
\synopsis{S-Lang version of gsl_sf_synchrotron_1}
\usage{Double_Type[] synchrotron_1 (Double_Type[] x)}
\done

\function{synchrotron_2}
\synopsis{S-Lang version of gsl_sf_synchrotron_2}
\usage{Double_Type[] synchrotron_2 (Double_Type[] x)}
\done

\function{taylorcoeff}
\synopsis{S-Lang version of gsl_sf_taylorcoeff}
\usage{Double_Type[] taylorcoeff (Int_Type[] n, Double_Type[] x)}
\done

