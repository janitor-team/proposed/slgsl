\function_sect{Fast Fourier Transform Routines}

\function{_gsl_fft_complex}
\synopsis{Perform an N-d FFT}
\usage{y = _gsl_fft_complex (x, dir)}
\description
  This routine computes the FFT of an array \exmp{x} and returns the
  result.  The integer-valued parameter \exmp{dir} parameter specifies
  the direction of the transform.  A forward transform will be
  produced for positive values of \exmp{dir} and a reverse transform
  will be computed for negative values. 
  
  The result will be a complex array of the same size and
  dimensionality as the the input array.
\notes
  It is better to call this routine indirectly using the \sfun{fft}
  function.
\seealso{fft}
\done

\function{fft}
\synopsis{Perform an N-d FFT}
\usage{y = fft (x, dir)}
\description
  This routine computes the FFT of an array \exmp{x} and returns the
  result.  The integer-valued parameter \exmp{dir} parameter specifies
  the direction of the transform.  A forward transform will be
  produced for positive values of \exmp{dir} and a reverse transform
  will be computed for negative values. 
  
  The result will be a complex array of the same size and
  dimensionality as the the input array.
\notes
  This routine is currently a wrapper for the \exmp{_gsl_fft_complex}
  function.
\seealso{_gsl_fft_complex}
\done


\function{convolve}
\synopsis{Perform a convolution}
\usage{b = convolve (array, kernel)}
\description
  This function performs a convolution of the specified array and
  kernel using FFTs.  One of the following qualifiers may be used to
  indicate how the overlap of the kernel with the edges of the array
  are to be handled:
#v+
    pad=value        Pad the array with the specified value
    wrap             Use periodic boundary-conditions
    reflect          Reflect the pixels at the boundary
    nearest          Use the value of the nearest edge pixel
#v-
  The default behavior is to use pad=0.0.
\notes
  The current implementation utilizes ffts and will expand the array
  to the nearest multiple of small primes for run-time efficiency.  A
  future version may allow different methods to be used.
\seealso{fft, correlate}
\done

\function{correlate}
\synopsis{Perform a correlation}
\usage{b = correlate (array, kernel)}
\description
  This function performs a correlation of the specified array and
  kernel using FFTs.  One of the following qualifiers may be used to
  indicate how the overlap of the kernel with the edges of the array
  are to be handled:
#v+
    pad=value        Pad the array with the specified value
    wrap             Use periodic boundary-conditions
    reflect          Reflect the pixels at the boundary
    nearest          Use the value of the nearest edge pixel
#v-
  The default behavior is to use pad=0.0.
\notes
  The current implementation utilizes ffts and will expand the array
  to the nearest multiple of small primes for run-time efficiency.  A
  future version may allow different methods to be used.  
\seealso{fft, convolve}
\done
