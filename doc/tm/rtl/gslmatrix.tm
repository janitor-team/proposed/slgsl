\function_sect{Linear Algebra and Matrix-Oriented Routines}

\function{linalg_LU_decomp}
\synopsis{Factorize a square matrix into its LU decomposition}
\usage{(LU,p) = linalg_LU_decomp (A [,&signum])}
\description
  This routines returns the LU decomposition of the square matrix
  \exmp{A} such that \exmp{P#A == LU}.  See the corresponding GSL
  documentation for how \exmp{L} and \exmp{U} are stored in \exmp{LU},
  and how the permutation matrix \exmp{P} is defined.  For many
  applications, it is unnecessary to unpack the matrix \exmp{LU} into
  its separate components.
 
  If the optional argument \exmp{&signum} is given, upon return
  \exmp{signum} will be set to the sign of the permutation that relates
  \exmp{P} to the identity matrix.
\seealso{linalg_LU_det, linalg_LU_invert, linalg_LU_solve}
\done

\function{linalg_LU_det}
\synopsis{Compute the determinant of a matrix from its LU decomposition}
\usage{det = linalg_LU_det (LU, signum)}
\description
  This function computes the determinant of a matrix from its LU
  decomposition.  In the LU form, determinant is given by the product
  of the diagonal elements with the sign of the permutation.
#v+
     require ("gslmatrix");
     define determinant (A)
     {
        variable LU, sig;
        (LU,) = linalg_LU_decomp (A, &sig);
        return linalg_LU_det (LU,sig);
     }
#v-
\seealso{linalg_LU_lndet, linalg_LU_decomp, linalg_LU_invert, linalg_LU_solve}
\done

\function{linalg_LU_lndet}
\synopsis{Compute the log of a determinant using LU decomposition}
\usage{det = linalg_LU_lndet (LU)}
\description
  This function computes the natural logarithm of the determinant of a
  matrix from its LU decomposition.  In the LU form, determinant is
  given by the product of the diagonal elements with the sign of the
  permutation.  This function is useful for cases where the product of
  the diagonal elements would overflow.
\seealso{linalg_LU_det, linalg_LU_decomp, linalg_LU_solve, linalg_LU_invert}
\done

\function{linalg_LU_invert}
\synopsis{Compute the inverse of a matrix via its LU decomposition}
\usage{inv = linalg_LU_invert (LU, p)}
\description
  This function may be used to compute the inverse of a matrix from
  its LU decomposition.  For the purposes of inverting a set of linear
  equations, it is preferable to use the \ifun{linalg_LU_solve}
  function rather than inverting the equations via the inverse.
#v+
    define matrix_inverse (A)
    {
       return linalg_LU_invert (linalg_LU_decomp (A));
    }
#v-
\seealso{linalg_LU_decomp, linalg_LU_solve, linalg_LU_det}
\done

\function{linalg_LU_solve}
\synopsis{Solve a set of linear equations using LU decomposition}
\usage{x = linalg_LU_solve (LU, p, b)}
\description
  This function solves the square linear system of equations
  \exmp{A#x=b} for the vector \exmp{x} via the LU decomposition of
  \exmp{A}.
#v+
   define solve_equations (A, b)
   {
      return linalg_LU_solve (linalg_LU_decomp (A), b);
   }
#v-
\seealso{linalg_LU_decomp, linalg_LU_det, linalg_LU_invert}
\done

\function{linalg_QR_decomp}
\synopsis{Factor a matrix into its QR form}
\usage{(QR, tau) = linalg_QR_decomp(A)}
\description
  This function may be used to decompose a rectangular matrix into its
  so-called QR such that \exmp{A=Q#R} where \exmp{Q} is a square
  orthogonal matrix and \exmp{R} is a rectangular right-triangular
  matrix.
  
  The factor \exmp{R} encoded in the diagonal and
  upper-triangular elements of the first return value \exmp{QR}.  The
  matrix \exmp{Q} is encoded in the lower triangular part of \exmp{QR}
  and the vector \exmp{tau} via Householder vectors and coefficients.
  See the corresponding \GSL documentation for the details of the
  encoding.  For most uses encoding details are not required.
\seealso{linalg_QR_solve, }
\done

\function{linalg_QR_solve}
\synopsis{Solve a system of linear equations using QR decomposition}
\usage{x = linalg_QR_solve(QR, tau, b [,&residual])}
\description
  This function may be used to solve the linear system \exmp{A#x=b}
  using the \exmp{QR} decomposition of \exmp{A}.
 
  If the optional fourth argument is present (\exmp{&residual}), or if
  \exmp{QR} is not a square matrix, then the linear system will be
  solved in the least-squares sense by minimizing the (Euclidean) norm
  of \exmp{A#x-b}.  Upon return, the value of the variable
  \exmp{residual} is set to the the norm of \exmp{A#x-b}.
\notes
  \GSL has a separate function called \exmp{gsl_linalg_QR_lssolve} for
  computing this least-squares solution.  The \ifun{linalg_QR_solve}
  combines both \exmp{gsl_linalg_QR_lssolve} and
  \exmp{gsl_linalg_QR_solve} into a single routine.
\seealso{linalg_QR_decomp}
\done

\function{linalg_SV_decomp}
\synopsis{Perform a singular-value decomposition on a matrix}
\usage{(U,S,V) = linalg_SV_decomp(A)}
\description
  This function factors a MxN (M>=N) rectangular matrix \exmp{A} into
  three factors such that \exmp{A = U#S#transpose(V)}, where \exmp{S}
  is diagonal matrix containing the singular values of \exmp{A} and
  \exmp{V} is a square orthogonal matrix.  Since \exmp{S} is diagonal,
  it is returned as a 1-d array.
\seealso{linalg_SV_solve}
\done

\function{linalg_SV_solve}
\synopsis{Solve a linear system using Singular-Value Decomposition}
\usage{x = linalg_SV_solve (U,V,S,b)}
\description
  This function ``solves'' the linear system \exmp{A#x=b} using the
  SVD form of \exmp{A}.
\example
#v+
   define svd_solve (A, b)
   {
      variable U, V, S;
      (U,V,S) = linalg_SV_decomp (A);
      return linalg_SV_solve (U,V,S,b);
   }
#v-
\seealso{linalg_SV_decomp, linalg_QR_solve, linalg_LU_solve}
\done

\function{eigen_symmv}
\synopsis{Compute the eigenvalues and eigenvectors of a Hermitian matrix}
\usage{(eigvecs, eigvals)=eigen_symmv(A)}
\description
  This function computes the eigenvalues and eigenvectors of a
  Hermitian (or real-symmetric) square matrix \exmp{A}.  The
  eigenvalues are returned sorted on their absolute value (or norm) in
  descending order. 
\seealso{eigen_nonsymmv}
\done

\function{eigen_nonsymmv}
\synopsis{Compute the eigenvalues and eigenvectors of a matrix}
\usage{(eigvecs, eigvals)=eigen_nonsymmv(A)}
\description
  This function returns the eigenvalues and eigenvectors of a real
  non-symmetric matrix \exmp{A}.  As such quantities are in general
  complex, complex-valued arrays will be returned.  The eigenvalues
  are returned in descending order sorted upon norm.
\seealso{eigen_symmv}
\done
