\function_sect{Discrete Wavelet Transform Routines}

\function{wavelet_transform}
\synopsis{Perform an N-d Discrete Wavelet Transform}
\usage{w = wavelet_transform (x, dir)}
\description

  This routine computes the DWT of an array \exmp{x} and returns the
  result. The optional \exmp{dir} parameter specifies the direction of
  the transform.  A forward transform will be produced for positive
  values of \exmp{dir} (default value) and a reverse transform will be
  computed for negative values. Array dimension(s) must be an integer
  power of two.

  The result will be an array of the same size and dimensionality as
  the the input array.

  The following qualifiers may be used :
#v+
    type = DWT_HAAR|DWT_DAUBECHIES|DWT_BSPLINE
    k = value          Selects the specific member of the wavelet
                       family.
    centered           The centered forms of the wavelets align the
                       coefficients of the various sub-bands on edges.
    nsf                Choose the "non-standard" forms ordering of the
                       rows and columns in the two-dimensional wavelet
                       transform.
#v-

 The following wavelet types are implemented : Daubechies (k = 4, 6,
 ..., 20, with k even), Haar (k = 2), bsplines (k = 100*i + j are 103,
 105, 202, 204, 206, 208, 301, 303, 305 307, 309).

\done
