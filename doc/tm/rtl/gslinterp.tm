\function_sect{Interpolation Routines}

\function{interp_linear}
\synopsis{Linear Interpolation}
\usage{y = interp_linear (x, Double_Type xa[], Double_Type ya[])}
\description
  Use linear interpolation to determine the value at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_polynomial, interp_cspline, interp_cspline_periodic, interp_akima, interp_akima_periodic}
\done

\function{interp_polynomial}
\synopsis{Polynomial Interpolation}
\usage{y = interp_polynomial (x, Double_Type xa[], Double_Type ya[])}
\description
  Use polynomial interpolation to determine the value at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
  
  The degree of the interpolating polynomial is given by one less than
  the number of points in the \var{xa} array.  For example, if
  \exmp{length(xa)} is 3, then a quadratic polynomial will be used.
\seealso{interp_linear, interp_cspline, interp_cspline_periodic, interp_akima, interp_akima_periodic}
\done

\function{interp_cspline}
\synopsis{Cubic Spline Interpolation}
\usage{y = interp_cspline (x, Double_Type xa[], Double_Type ya[])}
\description
  Use cubic spline interpolation with natural boundary conditions to
  determine the value at \var{x} given the points (\var{xa},
  \var{ya}).  The first argument, \var{x}, may be either a scalar or
  an array, and a result of the corresponding type will be returned.
\seealso{interp_linear, interp_polynomial, interp_cspline_periodic, interp_akima, interp_akima_periodic}
\done

\function{interp_cspline_periodic}
\synopsis{Cubic spline interpolation with periodic boundary conditions}
\usage{y = interp_cspline_periodic (x, Double_Type xa[], Double_Type ya[])}
\description
  Use cubic spline interpolation with periodic boundary conditions to
  determine the value at \var{x} given the points (\var{xa},
  \var{ya}).  The first argument, \var{x}, may be either a scalar or
  an array, and a result of the corresponding type will be returned.
\seealso{interp_linear, interp_polynomial, interp_cspline, interp_akima, interp_akima_periodic}
\done

\function{interp_akima}
\synopsis{Akima spline interpolation}
\usage{y = interp_akima (x, Double_Type xa[], Double_Type ya[])}
\description
  Use an Akima spline with natural boundary conditions to
  determine the value at \var{x} given the points (\var{xa},
  \var{ya}).  The first argument, \var{x}, may be either a scalar or
  an array, and a result of the corresponding type will be returned.
\seealso{interp_linear, interp_polynomial, interp_cspline, interp_cspline_periodic, interp_akima_periodic}
\done

\function{interp_akima_periodic}
\synopsis{Akima spline interpolation with periodic boundary conditions}
\usage{y = interp_akima_periodic (x, Double_Type xa[], Double_Type ya[])}
\description
  Use an Akima spline with periodic boundary conditions to
  determine the value at \var{x} given the points (\var{xa},
  \var{ya}).  The first argument, \var{x}, may be either a scalar or
  an array, and a result of the corresponding type will be returned.
\seealso{interp_linear, interp_polynomial, interp_cspline, interp_cspline_periodic, interp_akima}
\done

#% --------------------------------------------------------------------

\function_sect{First Derivative via Interpolation}

\function{interp_linear_deriv}
\synopsis{Compute derivative using linear interpolation}
\usage{y = interp_linear_deriv (x, Double_Type xa[], Double_Type ya[])}
\description
  Use linear interpolation to determine the value of the first
  derivative at \var{x} given the points (\var{xa}, \var{ya}).  The
  first argument, \var{x}, may be either a scalar or an array, and a
  result of the corresponding type will be returned.
\seealso{interp_polynomial_deriv, interp_cspline_deriv, interp_cspline_periodic_deriv, interp_akima_deriv, interp_akima_periodic_deriv}
\done

\function{interp_polynomial_deriv}
\synopsis{Compute derivative using polynomial interpolation}
\usage{y = interp_polynomial_deriv (x, Double_Type xa[], Double_Type ya[])}
\description
  Use polynomial interpolation to determine the value of the first
  derivative at \var{x} given the points (\var{xa}, \var{ya}).  The
  first argument, \var{x}, may be either a scalar or an array, and a
  result of the corresponding type will be returned.
  
  The degree of the interpolating polynomial is given by one less than
  the number of points in the \var{xa} array.  For example, if
  \exmp{length(xa)} is 3, then a quadratic polynomial will be used.  
\seealso{interp_linear_deriv, interp_cspline_deriv, interp_cspline_periodic_deriv, interp_akima_deriv, interp_akima_periodic_deriv}
\done

\function{interp_cspline_deriv}
\synopsis{Compute derivative using a cubic spline}
\usage{y = interp_cspline_deriv (x, Double_Type xa[], Double_Type ya[])}
\description
  Use cubic spline interpolation with natural boundary conditions to
  determine the value of the first derivative at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_linear_deriv, interp_polynomial_deriv, interp_cspline_periodic_deriv, interp_akima_deriv, interp_akima_periodic_deriv}
\done

\function{interp_cspline_periodic_deriv}
\synopsis{Compute derivative using a cubic spline}
\usage{y = interp_cspline_periodic_deriv (x, Double_Type xa[], Double_Type ya[])}
\description
  Use cubic spline interpolation with periodic boundary conditions to
  determine the value of the first derivative at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_linear_deriv, interp_polynomial_deriv, interp_cspline_deriv, interp_akima_deriv, interp_akima_periodic_deriv}
\done


\function{interp_akima_deriv}
\synopsis{Compute derivative using an Akima spline}
\usage{y = interp_akima_deriv (x, Double_Type xa[], Double_Type ya[])}
\description
  Use Akima spline interpolation with natural boundary conditions to
  determine the value of the first derivative at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_linear_deriv, interp_polynomial_deriv, interp_cspline_deriv, interp_cspline_periodic_deriv, interp_akima_periodic_deriv}
\done


\function{interp_akima_periodic_deriv}
\synopsis{Compute derivative using an Akima spline}
\usage{y = interp_cspline_deriv (x, Double_Type xa[], Double_Type ya[])}
\description
  Use Akima spline interpolation with periodic boundary conditions to
  determine the value of the first derivative at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_linear_deriv, interp_polynomial_deriv, interp_cspline_deriv, interp_cspline_periodic_deriv, interp_akima_deriv}
\done


#% --------------------------------------------------------------------

\function_sect{Second Derivative via Interpolation}

\function{interp_linear_deriv2}
\synopsis{Compute second derivative using linear interpolation}
\usage{y = interp_linear_deriv2 (x, Double_Type xa[], Double_Type ya[])}
\description
  Use linear interpolation to determine the value of the second
  derivative at \var{x} given the points (\var{xa}, \var{ya}).  The
  first argument, \var{x}, may be either a scalar or an array, and a
  result of the corresponding type will be returned.
\seealso{interp_polynomial_deriv2, interp_cspline_deriv2, interp_cspline_periodic_deriv2, interp_akima_deriv2, interp_akima_periodic_deriv2}
\done

\function{interp_polynomial_deriv2}
\synopsis{Compute second derivative using polynomial interpolation}
\usage{y = interp_polynomial_deriv2 (x, Double_Type xa[], Double_Type ya[])}
\description
  Use polynomial interpolation to determine the value of the second
  derivative at \var{x} given the points (\var{xa}, \var{ya}).  The
  first argument, \var{x}, may be either a scalar or an array, and a
  result of the corresponding type will be returned.
  
  The degree of the interpolating polynomial is given by one less than
  the number of points in the \var{xa} array.  For example, if
  \exmp{length(xa)} is 3, then a quadratic polynomial will be used.  
\seealso{interp_linear_deriv2, interp_cspline_deriv2, interp_cspline_periodic_deriv2, interp_akima_deriv2, interp_akima_periodic_deriv2}
\done

\function{interp_cspline_deriv2}
\synopsis{Compute second derivative using a cubic spline}
\usage{y = interp_cspline_deriv2 (x, Double_Type xa[], Double_Type ya[])}
\description
  Use cubic spline interpolation with natural boundary conditions to
  determine the value of the second derivative at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_linear_deriv2, interp_polynomial_deriv2, interp_cspline_periodic_deriv2, interp_akima_deriv2, interp_akima_periodic_deriv2}
\done

\function{interp_cspline_periodic_deriv2}
\synopsis{Compute second derivative using a cubic spline}
\usage{y = interp_cspline_periodic_deriv2 (x, Double_Type xa[], Double_Type ya[])}
\description
  Use cubic spline interpolation with periodic boundary conditions to
  determine the value of the second derivative at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_linear_deriv2, interp_polynomial_deriv2, interp_cspline_deriv2, interp_akima_deriv2, interp_akima_periodic_deriv2}
\done


\function{interp_akima_deriv2}
\synopsis{Compute second derivative using an Akima spline}
\usage{y = interp_akima_deriv2 (x, Double_Type xa[], Double_Type ya[])}
\description
  Use Akima spline interpolation with natural boundary conditions to
  determine the value of the second derivative at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_linear_deriv2, interp_polynomial_deriv2, interp_cspline_deriv2, interp_cspline_periodic_deriv2, interp_akima_periodic_deriv2}
\done


\function{interp_akima_periodic_deriv2}
\synopsis{Compute second derivative using an Akima spline}
\usage{y = interp_cspline_deriv2 (x, Double_Type xa[], Double_Type ya[])}
\description
  Use Akima spline interpolation with periodic boundary conditions to
  determine the value of the second derivative at \var{x} given the
  points (\var{xa}, \var{ya}).  The first argument, \var{x}, may be
  either a scalar or an array, and a result of the corresponding type
  will be returned.
\seealso{interp_linear_deriv2, interp_polynomial_deriv2, interp_cspline_deriv2, interp_cspline_periodic_deriv2, interp_akima_deriv2, interp_akima_periodic_deriv2}
\done
#% --------------------------------------------------------------------

\function_sect{Integration via Interpolation}

\function{interp_linear_integ}
\synopsis{Compute an integral using linear interpolation}
\usage{y = interp_linear_integ (Double_Type xa[], Double_Type ya[], a, b)}
\description
  This function computes the integral from \var{a} to \var{b} of the
  linear interpolating function associated with the set of points
  (\var{xa}, \var{ya}).  See \ifun{interp_linear} for more
  information about the interpolating function.
\seealso{interp_polynomial_integ, interp_cspline_integ, interp_cspline_periodic_integ, interp_akima_integ, interp_akima_periodic_integ}
\done

\function{interp_polynomial_integ}
\synopsis{Compute an integral using polynomial interpolation}
\usage{y = interp_polynomial_integ (Double_Type xa[], Double_Type ya[], a, b)}
\description
  This function computes the integral from \var{a} to \var{b} of the
  polynomial interpolating function associated with the set of points
  (\var{xa}, \var{ya}).  See \ifun{interp_polynomial} for more
  information about the interpolating function.
\seealso{interp_linear_integ, interp_cspline_integ, interp_cspline_periodic_integ, interp_akima_integ, interp_akima_periodic_integ}
\done

\function{interp_cspline_integ}
\synopsis{Compute an integral using a cubic spline}
\usage{y = interp_cspline_integ (Double_Type xa[], Double_Type ya[], a, b)}
\description
  This function computes the integral from \var{a} to \var{b} of the
  cubic spline interpolating function associated with the set of points
  (\var{xa}, \var{ya}).  See \ifun{interp_cspline} for more
  information about the interpolating function.
\seealso{interp_linear_integ, interp_polynomial_integ, interp_cspline_periodic_integ, interp_akima_integ, interp_akima_periodic_integ}
\done

\function{interp_cspline_periodic_integ}
\synopsis{Compute an integral using a cubic spline}
\usage{y = interp_cspline_periodic_integ (Double_Type xa[], Double_Type ya[], a, b)}
\description
  This function computes the integral from \var{a} to \var{b} of the
  cubic spline interpolating function associated with the set of points
  (\var{xa}, \var{ya}).  See \ifun{interp_cspline_periodic} for more
  information about the interpolating function.
\seealso{interp_linear_integ, interp_polynomial_integ, interp_cspline_integ, interp_akima_integ, interp_akima_periodic_integ}
\done


\function{interp_akima_integ}
\synopsis{Compute an integral using an Akima spline}
\usage{y = interp_akima_integ (Double_Type xa[], Double_Type ya[], a, b)}
\description
  This function computes the integral from \var{a} to \var{b} of the
  Akima spline interpolating function associated with the set of points
  (\var{xa}, \var{ya}).  See \ifun{interp_akima} for more
  information about the interpolating function.
\seealso{interp_linear_integ, interp_polynomial_integ, interp_cspline_integ, interp_cspline_periodic_integ, interp_akima_periodic_integ}
\done


\function{interp_akima_periodic_integ}
\synopsis{Compute an integral using an Akima spline}
\usage{y = interp_akima_periodic_integ (Double_Type xa[], Double_Type ya[], a, b)}
\description
  This function computes the integral from \var{a} to \var{b} of the
  Akima spline interpolating function associated with the set of points
  (\var{xa}, \var{ya}).  See \ifun{interp_akima_periodic} for more
  information about the interpolating function.
\seealso{interp_linear_integ, interp_polynomial_integ, interp_cspline_integ, interp_cspline_periodic_integ, interp_akima_integ}
\done

#% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\function_sect{Low-level Interpolation Routines}

\function{interp_linear_init}
\synopsis{Compute a linear interpolation object}
\usage{GSL_Interp_Type interp_linear_init (Double_Type_Type xa[], Double_Type_Type ya[])}
\description
  This function computes an interpolation object appropriate for
  linear interpolation on the specified \var{xa} and \var{ya} arrays.
\seealso{interp_eval, interp_polynomial_init, interp_cspline_init, interp_cspline_periodic_init, interp_akima_init, interp_akima_periodic_init}
\done

\function{interp_polynomial_init}
\synopsis{Compute a polynomial interpolation object}
\usage{GSL_Interp_Type interp_polynomial_init (Double_Type xa[], Double_Type ya[])}
\description
  This function computes an interpolation object appropriate for
  polynomial interpolation on the specified \var{xa} and \var{ya} arrays.
\seealso{interp_eval, interp_linear_init, interp_cspline_init, interp_cspline_periodic_init, interp_akima_init, interp_akima_periodic_init}
\done

\function{interp_cspline_init}
\synopsis{Compute a cubic spline Interpolation object}
\usage{GSL_Interp_Type interp_cspline_init (Double_Type xa[], Double_Type ya[])}
\description
  This function computes an interpolation object appropriate for
  cubic spline interpolation with natural boundary conditions on the
  specified \var{xa} and \var{ya} arrays. 
\seealso{interp_eval, interp_linear_init, interp_polynomial_init, interp_cspline_periodic_init, interp_akima_init, interp_akima_periodic_init}
\done

\function{interp_cspline_periodic_init}
\synopsis{Compute a cubic spline interpolation object}
\usage{GSL_Interp_Type interp_cspline_periodic_init (Double_Type xa[], Double_Type ya[])}
\description
  This function computes an interpolation object appropriate for
  cubic spline interpolation with periodic boundary conditions on the
  specified \var{xa} and \var{ya} arrays. 
\seealso{interp_eval, interp_linear_init, interp_polynomial_init, interp_cspline_init, interp_akima_init, interp_akima_periodic_init}
\done

\function{interp_akima_init}
\synopsis{Compute an Akima spline interpolation object}
\usage{GSL_Interp_Type interp_akima_init (Double_Type xa[], Double_Type ya[])}
\description
  This function computes an interpolation object appropriate for
  Akima spline interpolation with natural boundary conditions on the
  specified \var{xa} and \var{ya} arrays. 
\seealso{interp_eval, interp_linear_init, interp_polynomial_init, interp_cspline_init, interp_cspline_periodic_init, interp_akima_periodic_init}
\done

\function{interp_akima_periodic_init}
\synopsis{Compute an Akima spline interpolation object}
\usage{GSL_Interp_Type interp_akima_periodic_init (Double_Type xa[], Double_Type ya[])}
\description
  This function computes an interpolation object appropriate for
  Akima spline interpolation with periodic boundary conditions on the
  specified \var{xa} and \var{ya} arrays. 
\seealso{interp_eval, interp_linear_init, interp_polynomial_init, interp_cspline_init, interp_cspline_periodic_init, interp_akima_periodic}
\done

\function{interp_eval}
\synopsis{Evaluate an interpolation object}
\usage{y = interp_eval (GSL_Interp_Type c, x)}
\description
 Use the precomputed interpolation object \var{c} to interpolate its
 value at \var{x}, which may be either
 a scalar or an array.  An interpolated value of the corresponding
 shape will be returned.
\seealso{interp_linear_init, interp_eval_deriv, interp_eval_deriv2,
 interp_eval_integ}
\done 

\function{interp_eval_deriv}
\synopsis{Evaluate the derivative of an interpolation object}
\usage{dydx = interp_eval_deriv (GSL_Interp_Type c, x)}
\description
 Use the precomputed interpolation object \var{c} to interpolate its
 first derivative at \var{x}, which may be either
 a scalar or an array.  An interpolated value of the corresponding
 shape will be returned.
\seealso{interp_linear_init, interp_eval, interp_eval_deriv2, interp_eval_integ}
\done

\function{interp_eval_deriv2}
\synopsis{Evaluate the derivative of an interpolation object}
\usage{d2ydx2 = interp_eval_deriv2 (GSL_Interp_Type c, x)}
\description
 Use the precomputed interpolation object \var{c} to interpolate its
 second derivative at \var{x}, which may be either
 a scalar or an array.  An interpolated value of the corresponding
 shape will be returned.
\seealso{interp_linear_init, interp_eval, interp_eval_deriv, interp_eval_integ}
\done

\function{interp_eval_integ}
\synopsis{Compute the integral of an interpolation object}
\usage{integral = interp_eval_integ (GSL_Interp_Type c, a, b)}
\description
 Use the precomputed interpolation object \var{c} to interpolate its
 integral from \var{a} to \var{b}.
\seealso{interp_linear_init, interp_eval, interp_eval_deriv, interp_eval_deriv2}
\done
