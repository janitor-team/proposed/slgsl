#!/usr/bin/env slsh

% Usage: wavelet2d.sl image.png image-low.png
% image.png being a 8-bit gray PNG image

require ("gsl");
require ("png");

define slsh_main ()
{
   if (__argc != 2)
     {
	() = fprintf (stderr, "\
Usage: wavelet2d.sl image.png image-low.png\n\
  where image.png is an 8-but grayscale PNG image\n");
	exit (1);
     }

   variable x, w, k = 8;

   x = png_read (__argv [1]);
   variable dims = array_shape (x), nx = dims[1], ny=dims[0];

   w = wavelet_transform (x [[0:ny-1], [0:nx-1]]
			  ; type = DWT_DAUBECHIES, k = k);

   % all but the upper-left n/k of dwt is set to 0
   w [[ny / k:], [0:nx / k]] = 0;
   w [*, [nx / k:]] = 0;
   x = wavelet_transform (w, -1 ;
			  type = DWT_DAUBECHIES, k = k);
   png_write (__argv [2], typecast (x, UChar_Type));
}
