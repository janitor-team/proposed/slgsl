% This example computes an approximation of a sin curve,
% setting all but the 10 largest largest components
% of the wavelet transform to 0

require ("gsl");
require ("csv");

define slsh_main ()
{
   variable x, y, w, a, t, fp, N = 1024;

   t = [0:1:#N];
   x = sin (2 * PI * t);
   w = wavelet_transform (x ; type = DWT_DAUBECHIES, k = 4);
   a = array_sort (abs (w));
   w [a [[:N - 10]]] = 0;
   y = wavelet_transform (w, -1 ; type = DWT_DAUBECHIES, k = 4);
   csv_writecol ("dwt.tsv", t, x, y; delim='\t');
   fp = fopen ("dwt.gp", "w");
   () = fprintf (fp, "p 'dwt.tsv' using 1:2 w l, '' using 1:3 w l\n");
   () = fprintf (fp, "pause -1\n");
   () = fclose (fp);
   message ("Now run : gnuplot dwt.gp");
}
